<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;
use dosamigos\tinymce\TinyMce;
use dosamigos\fileinput\FileInput;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model backend\models\Basic */
/* @var $form yii\widgets\ActiveForm */
$model->status = $model->isNewRecord ? 1 : $model->status;
$js = '
jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {
        jQuery(this).html("Image: " + (index + 1))
    });
    jQuery(".dynamicform_wrapper .fileinput-new img").last().attr("src", "");
  
});

jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {
        jQuery(this).html("Image: " + (index + 1))
    });
});
';

$this->registerJs($js);
?>
      <div class="row">
    <?php $form = ActiveForm::begin(['id' => 'dynamic-form','options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="col-md-10">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Details</h3>
            </div>
            <div class="box-body">
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'description')->widget(TinyMce::className(), [
    'options' => ['rows' => 14],
    'language' => 'en_CA',
    'clientOptions' => [
        'plugins' => [
            "lists",
            "code",
            "contextmenu paste"
        ],
        'toolbar' => "undo redo | styleselect | bold italic | bullist numlist outdent"
    ]
]);?>
  <?= $form->field($model, 'other')->widget(TinyMce::className(), [
    'options' => ['rows' => 14],
    'language' => 'en_CA',
    'clientOptions' => [
        'plugins' => [
            "lists",
            "code",
            "contextmenu paste"
        ],
        'toolbar' => "undo redo | styleselect | bold italic | bullist numlist outdent"
    ]
]);?>
<?= $form->field($model, 'tags')->textInput(['maxlength' => true]) ?>
<div class="padding-v-md">
        <div class="line line-dashed"></div>
    </div>
    <?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
        'widgetBody' => '.container-items', // required: css class selector
        'widgetItem' => '.item', // required: css class
        'limit' => 6, // the maximum times, an element can be cloned (default 999)
        'min' => 0, // 0 or 1 (default 1)
        'insertButton' => '.add-item', // css class
        'deleteButton' => '.remove-item', // css class
        'model' => $images[0],
        'formId' => 'dynamic-form',
        'formFields' => [
            'title',
            'alt',
            'image',
        ],
    ]); ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-envelope"></i> Solar Solution Images(Max 6)
            <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Image</button>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body container-items"><!-- widgetContainer -->
            <?php foreach ($images as $index => $image): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <span class="panel-title-address">Image: <?= ($index + 1) ?></span>
                        <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                            // necessary for update action.
                            if (!$image->isNewRecord) {
                                echo Html::activeHiddenInput($image, "[{$index}]id");
                            }
                        ?>

                        <div class="row">
                            <div class="col-sm-6">
                                  <?= $form->field($image, "[{$index}]node_image_title")->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($image, "[{$index}]file")->widget(\dosamigos\fileinput\FileInput::className(), [
    'options' => ['accept' => 'image/*'],
        'thumbnail' => $image->getAvatarImage(),
    'style' => \dosamigos\fileinput\FileInput::STYLE_IMAGE,
])->hint('');?>
                            </div>
                        </div><!-- end:row -->

                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?php DynamicFormWidget::end(); ?>

      <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

</div>
          </div>
        </div>
            <?php ActiveForm::end(); ?>
      </div>
<?php
