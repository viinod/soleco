<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;


/* @var $this yii\web\View */
/* @var $model backend\models\Gallery */
$this->title = 'Update Sub Content: ' . $model->sub_title;;
$this->params['breadcrumbs'][] = ['label' => 'Advanced Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $node->title, 'url' => ['list', 'nid' => $node->id]];
$this->params['breadcrumbs'][] = ['label' => $model->sub_title, 'url' => ['see', 'id' => $model->id, 'nid' => $node->id]];
$this->params['breadcrumbs'][] = 'Update';

?>
  <h1><?= Html::encode($this->title) ?></h1>
<div class="row">
<div class="col-md-10">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'sub_title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'icon')->widget(\noam148\imagemanager\components\ImageManagerInputWidget::className(), [
                'aspectRatio' => (16/9), //set the aspect ratio
                'cropViewMode' => 0, //crop mode, option info: https://github.com/fengyuanchen/cropper/#viewmode
                'showPreview' => true, //false to hide the preview
                'showDeletePickedImageConfirm' => true, //on true show warning before detach image
            ]); ?> 
  <?= $form->field($model, 'sub_description')->widget(TinyMce::className(), [
    'options' => ['rows' => 9],
    'language' => 'en_CA',
    'clientOptions' => [
        'plugins' => [
            "lists",
            "code",
            "contextmenu paste"
        ],
        'toolbar' => "undo redo | styleselect | bold italic | bullist numlist outdent"
    ]
]);?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>
   