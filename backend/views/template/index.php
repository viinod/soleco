<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Templates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="template-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin(); ?>

<?= $form->field($model,'type')->dropDownList(
                                ArrayHelper::map(\backend\models\Page::find()->select(['ctype'])->groupBy(['ctype'])->asArray()->all(), 'ctype', 'ctype'),
                                [
                                    'prompt'=>'Select Content Type',
                                ]); ?>
<?= $form->field($model, 'style')->dropDownList([ 'list' => 'list', 'detail' => 'detail'],['prompt' => 'Please select']); ?>

<?= $form->field($model,'file')->dropDownList(
                                $templates,
                                [
                                    'prompt'=>'Select Content Type',
                                ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'type',
            'style',
            'file',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
