<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
      <div class="row">
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">File Editor :- <?=$file?></h3>
            </div>
            <div class="box-body">
<?= Html::beginForm(['template/layout', 'file' => $file], 'post', ['enctype' => 'multipart/form-data']) ?>
<div class="form-group">
<label class="control-label" >Content</label>
<?= Html::textarea('content',$fcontent,['class' => 'form-control','rows' => 10,'id' => 'code']) ?>
 </div>
 <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
  <?= Html::a('Cancel',Url::to(['template/index']), ['class' => 'btn btn-primary']) ?>
<?= Html::endForm() ?>    

			</div>
          </div>
        </div>
        <div class="col-md-3">
        <?php
        foreach ($templates as $template) {
        	echo Html::a($template, ['template/layout', 'file' => $template])."<br />";
        }
        ?>
        </div>
      </div>


<?php
$script = <<< JS
      var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
        lineNumbers: true,
        matchBrackets: true,
        mode: "application/x-httpd-php",
        indentUnit: 4,
        indentWithTabs: true
      });
JS;
$this->registerJs($script);
?>


