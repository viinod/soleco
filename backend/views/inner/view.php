<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Banner */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Banners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

      <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'mainTitle',
            [
            'attribute'=>'inner',
            'value'=>$model->inner != null ? Yii::getAlias('@url').'/innerbanners/'.$model->inner : "",
            'format' => ['image',['width'=>'100']],
            ],
            // 'video',
            // 'price',
            // 'link',
            // 'setting',
            // 'digit1',
            // 'digit2',
            // 'digit3',
            // 'digit4',
            // 'digit5',
            // 'field1',
            // 'field2',
            // 'field3',
            // 'field4',
            // 'field5',
            // 'text1:ntext',
            // 'text2:ntext',
            // 'text3:ntext',
            // 'text4:ntext',
            // 'text5:ntext',
            // 'started_at',
            // 'ended_at',
            // 'created_at',
            // 'updated_at',
            // [
            // 'attribute' => 'created_at',
            // 'value' => Yii::$app->dateformatter->getDTFormat($model->created_at),
            // ],
            // [
            // 'attribute' => 'updated_at',
            // 'value' => Yii::$app->dateformatter->getDTFormat($model->updated_at),
            // ],
            // [
            // 'attribute' => 'created_by',
            // 'value' => isset($model->createdBy->username) ? $model->createdBy->username : "",
            // ],
            // [
            // 'attribute' => 'updated_by',
            // 'value' => isset($model->updatedBy->username) ? $model->updatedBy->username : "",
            // ],
            //'status:boolean',
            // 'menu',
            // 'type',
            // 'slug',
        ],
    ]) ?>

</div>
