<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;

?>
      <div class="row">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="col-md-10">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Details</h3>
            </div>
            <div class="box-body">
    <?= Html::activeHiddenInput($model, 'title');?>
<?= $form->field($model, 'img')->widget(\dosamigos\fileinput\FileInput::className(), [
    'options' => ['accept' => 'image/*'],
        'thumbnail' => $model->getAvatarImage(),
    'style' => \dosamigos\fileinput\FileInput::STYLE_IMAGE,
])->hint('Proper dimension is 1974(width)x589(height)');?>

      <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>
          </div>
        </div>
            <?php ActiveForm::end(); ?>
      </div>
