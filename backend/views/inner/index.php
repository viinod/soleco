<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\BannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inner Banners';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'mainTitle',
            [
             'attribute' => 'inner',
             'value' => function($data) {
                    return Yii::getAlias('@url').'/innerbanners/'.$data->inner;
                },
            'format' => ['image',['width'=>'80']],
            ],
            // 'summary:ntext',
            // 'description:ntext',
            // 'other:ntext',
            // 'image',
            // 'video',
            // 'price',
            // 'link',
            // 'setting',
            // 'digit1',
            // 'digit2',
            // 'digit3',
            // 'digit4',
            // 'digit5',
            // 'field1',
            // 'field2',
            // 'field3',
            // 'field4',
            // 'field5',
            // 'text1:ntext',
            // 'text2:ntext',
            // 'text3:ntext',
            // 'text4:ntext',
            // 'text5:ntext',
            // 'started_at',
            // 'ended_at',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
            //'status:boolean',
            // 'menu',
            //'type',
            // 'slug',

                     [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
                // 'urlCreator' => function($action, $model, $key, $index) { 
                //     return yii\helpers\Url::to([$action,'gid' => $model->id]);
                // },
            ],
        ],
    ]); ?>
</div>
