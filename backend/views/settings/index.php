<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Base Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<?php
$form = ActiveForm::begin();
 foreach ($settings as $index => $setting) {
            echo $form->field($setting, "[$index]value")->label($setting->title);
 }
 ?>
<div class="form-group">
    <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
</div>
<?php
ActiveForm::end();