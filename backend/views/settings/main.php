<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<?php
$form = ActiveForm::begin();
 foreach ($settings as $index => $setting) {
        if($setting->name == 'address' or $setting->name == 'about' or $setting->name == 'google map' or $setting->name == 'analytics' or $setting->name == 'seo keywords' or $setting->name == 'seo description'){
            echo $form->field($setting, "[$index]value")->textarea(['rows' => '4'])->label($setting->title);
    }
     else{
            echo $form->field($setting, "[$index]value")->label($setting->title);
    }
 }
 ?>
<div class="form-group">
    <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
</div>
<?php
ActiveForm::end();