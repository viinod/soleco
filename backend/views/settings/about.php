<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\fileinput\FileInput;
/* @var $this yii\web\View */
/* @var $model backend\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>



<?= $form->field($model, 'file')->widget(\dosamigos\fileinput\FileInput::className(), [
    'options' => ['accept' => 'image/*'],
])->label('About Us Image');?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
<?php if(!empty(Yii::$app->params['settings']['about']['about'])): ?>
<img src="<?=Yii::getAlias('@siteUrl').'/about/'.Yii::$app->params['settings']['about']['about']?>" width="300px">
<?=Yii::getAlias('@siteUrl').'/about/'.Yii::$app->params['settings']['about']['about']?>
<?php endif; ?>
</div>
