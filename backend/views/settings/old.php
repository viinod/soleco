<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin();
$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<?php
foreach ($settings as $index => $setting) {
    if($setting->name == 'address' or $setting->name == 'about' or $setting->name == 'google map'){
    echo $form->field($setting, "[$index]value")->textarea(['rows' => '4'])->label(ucfirst($setting->name));
     echo $form->field($setting, "[$index]id")->hiddenInput()->label(false);
    }else{
    echo $form->field($setting, "[$index]value")->label(ucfirst($setting->name));
     echo $form->field($setting, "[$index]id")->hiddenInput()->label(false);
    }
}
?>
 <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
    </div>
<?php
ActiveForm::end();
