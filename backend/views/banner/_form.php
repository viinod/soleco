<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;

/* @var $this yii\web\View */
/* @var $model backend\models\Basic */
/* @var $form yii\widgets\ActiveForm */
$model->status = $model->isNewRecord ? 1 : $model->status;
?>
      <div class="row">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="col-md-10">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Details</h3>
            </div>
            <div class="box-body">
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'img')->widget(\dosamigos\fileinput\FileInput::className(), [
    'options' => ['accept' => 'image/*'],
        'thumbnail' => $model->getAvatarImage(),
    'style' => \dosamigos\fileinput\FileInput::STYLE_IMAGE,
])->hint('Proper dimension is 1630(width)x721(height)');?>
  <?=$form->field($model, 'status')->widget(CheckboxX::classname(), [
        'autoLabel' => true,
              'pluginOptions' => [
              'size'=>'sm',
              'threeState'=>false,
              'theme' => 'krajee-flatblue',
          ]
  ])->label(false);
  ?>
      <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>
          </div>
        </div>
            <?php ActiveForm::end(); ?>
      </div>
