<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\SourceMessage;

/* @var $this yii\web\View */
/* @var $model backend\models\Message */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = Yii::$app->request->get('id');
    $parent = SourceMessage::findOne($id);
    $model->key = $parent->message;
}
}else{
$model->key = $model->id0->message;	
}
?>
<div class="message-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'key')->textInput() ?>

    <?= $form->field($model, 'translation')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
