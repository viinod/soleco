<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Main Menu', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Pages',
                        'icon' => 'file-text-o ',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Banners', 'icon' => 'circle-o', 'url' => ['/banner']],
                            ['label' => 'Inner Banners', 'icon' => 'circle-o', 'url' => ['/inner']],
                            ['label' => 'Basic Pages', 'icon' => 'circle-o', 'url' => ['/basic']],
                            ['label' => 'Advanced Page', 'icon' => 'circle-o', 'url' => ['/advanced']],
                            ['label' => 'Partners', 'icon' => 'circle-o', 'url' => ['/partner']],
                            ['label' => 'Solutions', 'icon' => 'circle-o', 'url' => ['/solution']],
                            ['label' => 'Solar Solutions', 'icon' => 'circle-o', 'url' => ['/solar-solution']],
                            ['label' => 'Blocks', 'icon' => 'circle-o', 'url' => ['/block']],
                            ['label' => 'Feature Blocks', 'icon' => 'circle-o', 'url' => ['/feature']],                        
                        ],
                    ],
                    [
                        'label' => 'Template',
                        'icon' => 'clone',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Create Files', 'icon' => 'circle-o', 'url' => ['/template'],],
                            ['label' => 'Template Editor', 'icon' => 'circle-o', 'url' => ['/template/php'],],
                            ['label' => 'Layout Editor', 'icon' => 'circle-o', 'url' => ['/template/layout'],],
                            ['label' => 'Widget Editor', 'icon' => 'circle-o', 'url' => ['/template/widget'],],
                            ['label' => 'Css Editor', 'icon' => 'circle-o', 'url' => ['/template/css'],],
                            ['label' => 'Js Editor', 'icon' => 'circle-o', 'url' => ['/template/js'],],
                        ],
                    ],
                    ['label' => 'Keywords', 'icon' => 'th', 'url' => ['/keyword']],
                    [
                        'label' => 'Settings',
                        'icon' => 'gears',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Base Settings', 'icon' => 'circle-o', 'url' => ['/settings/index']],
                            ['label' => 'Custom Settings', 'icon' => 'circle-o', 'url' => ['/settings/custom']],
                            ['label' => 'SMTP Settings', 'icon' => 'circle-o', 'url' => ['/settings/smtp']],
                            ['label' => 'Image Settings', 'icon' => 'circle-o', 'url' => ['/settings/image']],
                       ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
