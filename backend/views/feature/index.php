<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Feature Blocks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Feature Block', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=  GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'mainTitle',
        //'category',
        ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>

</div>
