<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Block */

$this->title = 'Create Feature Block';
$this->params['breadcrumbs'][] = ['label' => 'Feature Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
            'block' => $block,
    ]) ?>

</div>
