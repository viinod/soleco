<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use dosamigos\tinymce\TinyMce;
$location = Yii::getAlias(Yii::$app->params['site.block']);
$file =  $location.'block--id-'.$block->id.'.php';
if(file_exists($file))
$block->block_template = file_get_contents($file);
/* @var $this yii\web\View */
/* @var $block app\modules\yii2extensions\models\Customer */
/* @var $block_images app\modules\yii2extensions\models\Address */
?>

<div class="customer-form">

     <?php $form = ActiveForm::begin(['id' => 'dynamic-form','options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($block, 'title')->textInput(['maxlength' => true]) ?>
             <?= $form->field($block, 'sub_title')->textInput(['maxlength' => true]) ?>
  <?= $form->field($block, 'description')->textArea(['rows' => 10]);?>
   <?= $form->field($block, 'url')->textInput(['maxlength' => true]) ?>
            <?= $form->field($block, 'icon')->widget(\noam148\imagemanager\components\ImageManagerInputWidget::className(), [
                'aspectRatio' => (16/9), //set the aspect ratio
                'cropViewMode' => 0, //crop mode, option info: https://github.com/fengyuanchen/cropper/#viewmode
                'showPreview' => true, //false to hide the preview
                'showDeletePickedImageConfirm' => true, //on true show warning before detach image
            ]); ?>  
        </div>
        <div class="col-sm-6">
            <?= $form->field($block, 'block_template')->textarea(['rows' => 15,'id' => 'code'])->hint("key ==> \\frontend\widgets\Block::widget(['id' => $block->id])") ?>  
        </div>   
    </div>

    <div class="form-group">
        <?= Html::submitButton($block->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< JS
      var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
        lineNumbers: true,
        matchBrackets: true,
        mode: "application/x-httpd-php",
        indentUnit: 4,
        indentWithTabs: true
      });
JS;
$this->registerJs($script);
?>