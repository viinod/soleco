<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Block */

$this->title = 'Update Feature Block:'.$block->title;
$this->params['breadcrumbs'][] = ['label' => 'Feature Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $block->title, 'url' => ['view', 'id' => $block->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="project-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
            'block' => $block,
    ]) ?>

</div>
