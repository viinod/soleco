<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\fileinput\FileInput;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Gallery */
$this->title = $node->title;
$this->params['breadcrumbs'][] = ['label' => 'Advanced Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['list', 'nid' => $node->id]];
?>
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Sub Content', ['add', 'nid' => $node->id], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            'sub_title',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{edit} {see} {thrash}',
                'buttons' => [
                'edit' => function ($url) {
                    return Html::a('&nbsp;<span class="glyphicon glyphicon-pencil"></span>', $url,['title' => 'Add']);
                },
                'see' => function ($url) {
                    return Html::a('&nbsp;<span class="glyphicon glyphicon-eye-open"></span>', $url,['title' => 'view']);
                },
        'thrash' => function($url){
            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                'class' => '',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]);
        }
   
                ],
                'urlCreator' => function($action, $model, $key, $index) { 
                    return yii\helpers\Url::to([$action,'nid' => $model->nid,'id' => $model->id]);
                },
            ],
        ],
    ]); ?>