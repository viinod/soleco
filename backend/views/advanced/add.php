<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;


/* @var $this yii\web\View */
/* @var $model backend\models\Gallery */
$this->title = 'Create Sub Content';
$this->params['breadcrumbs'][] = ['label' => 'Advanced Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $node->title, 'url' => ['list', 'gid' => $node->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
  <h1><?= Html::encode($this->title) ?></h1>
<div class="row">
<div class="col-md-10">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'sub_title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'summary')->textarea(['rows' => 4]) ?>
  <?= $form->field($model, 'description')->widget(TinyMce::className(), [
    'options' => ['rows' => 9],
    'language' => 'en_CA',
    'clientOptions' => [
        'plugins' => [
            "lists",
            "code",
            "contextmenu paste"
        ],
        'toolbar' => "undo redo | styleselect | bold italic | bullist numlist outdent"
    ]
]);?>
  <?= $form->field($model, 'other')->widget(TinyMce::className(), [
    'options' => ['rows' => 9],
    'language' => 'en_CA',
    'clientOptions' => [
        'plugins' => [
            "lists",
            "code",
            "contextmenu paste"
        ],
        'toolbar' => "undo redo | styleselect | bold italic | bullist numlist outdent"
    ]
]);?>
  <?= $form->field($model, 'list')->widget(TinyMce::className(), [
    'options' => ['rows' => 9],
    'language' => 'en_CA',
    'clientOptions' => [
        'plugins' => [
            "lists",
            "code",
            "contextmenu paste"
        ],
        'toolbar' => "undo redo | styleselect | bold italic | bullist numlist outdent"
    ]
]);?>
<?= $form->field($model, 'img')->widget(\dosamigos\fileinput\FileInput::className(), [
    'options' => ['accept' => 'image/*'],
        'thumbnail' => $model->getAvatarImage(),
    'style' => \dosamigos\fileinput\FileInput::STYLE_IMAGE,
])?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>
   