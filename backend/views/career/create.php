<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Basic */

$this->title = 'Create Career';
$this->params['breadcrumbs'][] = ['label' => 'Careers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="basic-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,

    ]) ?>

</div>
