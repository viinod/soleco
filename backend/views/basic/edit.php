<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;


/* @var $this yii\web\View */
/* @var $model backend\models\Gallery */
$this->title = 'Update Sub Content: ' . $model->node_image_title;;
$this->params['breadcrumbs'][] = ['label' => 'Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $node->title, 'url' => ['list', 'nid' => $node->id]];
$this->params['breadcrumbs'][] = ['label' => $model->node_image_title, 'url' => ['see', 'id' => $model->id, 'nid' => $node->id]];
$this->params['breadcrumbs'][] = 'Update';

?>
  <h1><?= Html::encode($this->title) ?></h1>
<?php if($node->link == 'sister') : ?>
    <?= $this->render('_sister', [
        'model' => $model,

    ]) ?>
<?php elseif($node->link == 'brand') : ?>
    <?= $this->render('_brand', [
        'model' => $model,

    ]) ?> 
<?php elseif($node->link == 'achieve') : ?>
    <?= $this->render('_achieve', [
        'model' => $model,

    ]) ?> 
<?php else : ?>
    <?= $this->render('_offer', [
        'model' => $model,

    ]) ?> 
<?php endif; ?>
   