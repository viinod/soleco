
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$model->date = $model->isNewRecord ? date('Y-m-d',time()) : date('Y-m-d',$model->date);
?>
<div class="row">
<div class="col-md-10">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'node_image_title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'date')->textInput(['maxlength' => true,'type' => 'date'])->label('Phone') ?>
<?= $form->field($model, 'sister')->widget(\dosamigos\fileinput\FileInput::className(), [
    'options' => ['accept' => 'image/*'],
        'thumbnail' => $model->getAvatarImage(),
    'style' => \dosamigos\fileinput\FileInput::STYLE_IMAGE,
])->hint('');?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>