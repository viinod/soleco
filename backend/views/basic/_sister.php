<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="row">
<div class="col-md-10">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'node_image_title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'node_image_description')->textInput(['maxlength' => true])->label('Address') ?>
    <?= $form->field($model, 'text1')->textInput(['maxlength' => true])->label('Phone') ?>
    <?= $form->field($model, 'text2')->textInput(['maxlength' => true])->label('Email') ?>
<?= $form->field($model, 'sister')->widget(\dosamigos\fileinput\FileInput::className(), [
    'options' => ['accept' => 'image/*'],
        'thumbnail' => $model->getAvatarImage(),
    'style' => \dosamigos\fileinput\FileInput::STYLE_IMAGE,
])->hint('');?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>