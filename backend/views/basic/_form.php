<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model backend\models\Basic */
/* @var $form yii\widgets\ActiveForm */
$model->status = $model->isNewRecord ? 1 : $model->status;
?>
      <div class="row">
        <?php $form = ActiveForm::begin(); ?>
        <div class="col-md-10">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Details</h3>
            </div>
            <div class="box-body">
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'sub_title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'summary')->textarea(['rows' => 4]) ?>
<?= $form->field($model, 'description')->widget(TinyMce::className(), [
    'options' => ['rows' => 12],
    'language' => 'en_CA',
    'clientOptions' => [
        'plugins' => [
            "lists",
            "code",
            "contextmenu paste"
        ],
        'toolbar' => "undo redo | styleselect | bold italic | bullist numlist outdent"
    ]
]);?>
      <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

</div>
          </div>
        </div>
            <?php ActiveForm::end(); ?>
      </div>
