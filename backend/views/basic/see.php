<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Article */

$this->title = $model->node_image_title;;
$this->params['breadcrumbs'][] = ['label' => 'Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $node->title, 'url' => ['list', 'nid' => $node->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['edit', 'id' => $model->id, 'nid' => $node->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['thrash', 'id' => $model->id, 'nid' => $node->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'node_image_title',
            [
                'label' => 'Address',
            'attribute'=>'node_image_description',            
            ],
            [
                'label' => 'Phone',
            'attribute'=>'text1',            
            ],
            [
                'label' => 'Email',
            'attribute'=>'text2',            
            ],
            [
            'attribute'=>'node_image',
            'value'=>$model->node_image != null ? Yii::getAlias('@url').'/nodeimages/'.$model->node_image : "",
            'format' => ['image',['width'=>'100']],
            ],
        ],
    ]) ?>

</div>
