<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "node".
 *
 * @property integer $id
 * @property string $title
 * @property string $summary
 * @property string $description
 * @property string $other
 * @property string $image
 * @property string $video
 * @property string $price
 * @property string $link
 * @property string $file
 * @property integer $setting
 * @property string $digit1
 * @property string $digit2
 * @property string $digit3
 * @property string $digit4
 * @property string $digit5
 * @property string $field1
 * @property string $field2
 * @property string $field3
 * @property string $field4
 * @property string $sub_title
 * @property string $text1
 * @property string $text2
 * @property string $text3
 * @property string $text4
 * @property string $text5
 * @property integer $started_at
 * @property integer $ended_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $status
 * @property integer $menu
 * @property string $type
 * @property string $slug
 *
 * @property NodeImage[] $nodeImages
 */
class Node extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'node';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['summary', 'description', 'other', 'text1', 'text2', 'text3', 'text4', 'text5'], 'string'],
            [['price', 'digit1', 'digit2', 'digit3', 'digit4', 'digit5'], 'number'],
            [['setting', 'started_at', 'ended_at', 'created_at', 'updated_at', 'created_by', 'updated_by', 'status', 'menu'], 'integer'],
            [['title', 'image', 'video', 'link', 'file', 'field1', 'field2', 'field3', 'field4', 'sub_title', 'type', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'summary' => 'Summary',
            'description' => 'Description',
            'other' => 'Other',
            'image' => 'Image',
            'video' => 'Video',
            'price' => 'Price',
            'link' => 'Link',
            'file' => 'File',
            'setting' => 'Setting',
            'digit1' => 'Digit1',
            'digit2' => 'Digit2',
            'digit3' => 'Digit3',
            'digit4' => 'Digit4',
            'digit5' => 'Digit5',
            'field1' => 'Field1',
            'field2' => 'Field2',
            'field3' => 'Field3',
            'field4' => 'Field4',
            'sub_title' => 'Sub_title',
            'text1' => 'Text1',
            'text2' => 'Text2',
            'text3' => 'Text3',
            'text4' => 'Text4',
            'text5' => 'Text5',
            'started_at' => 'Started At',
            'ended_at' => 'Ended At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'status' => 'Status',
            'menu' => 'Menu',
            'type' => 'Type',
            'slug' => 'Slug',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNodeImages()
    {
        return $this->hasMany(NodeImage::className(), ['nid' => 'id']);
    }
    public function getNodeSubs()
    {
        return $this->hasMany(NodeSub::className(), ['nid' => 'id']);
    }
    /**
     * Returns content and replace widgets short codes
     *
     * Widget short code example: [[\app\widgets\SomeWidget:method]]
     *
     * @return string
     */
    public function getContent()
    {
        $content = preg_replace_callback('/\[\[([^(\[\[)]+:[^(\[\[)]+)\]\]/is', [$this, 'replace'], $this->description);
        return $content;
    }
    /**
     * Replaces widget short code on appropriate widget
     *
     * @param $data
     *
     * @return string
     */
    private function replace($data)
    {
        $widget = explode(':', $data[1]);
        if (class_exists($class = $widget[0]) && method_exists($class, $method = $widget[1])) {
            return call_user_func([$class, $method]);
        }
        return '';
    }
}
