<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "setting".
 *
 * @property integer $id
 * @property string $title
 * @property string $module
 * @property string $name
 * @property string $value
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'module', 'name'], 'string', 'max' => 255],
            [['value'], 'string', 'max' => 510],
            ['file', 'image', 'extensions' => 'png, jpg'],
            ['title', 'required' ,'on' => 'custom-only' ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'file' => 'Logo',
        ];
    }

}
