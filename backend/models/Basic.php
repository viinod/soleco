<?php

namespace backend\models;

use Yii;
use common\models\User;
/**
 * This is the model class for table "node".
 *
 * @property integer $id
 * @property string $title
 * @property string $summary
 * @property string $description
 * @property string $other
 * @property string $image
 * @property string $video
 * @property string $price
 * @property string $link
 * @property integer $setting
 * @property integer $status
 * @property integer $menu
 * @property string $type
 * @property string $slug
 *
 * @property NodeImage[] $nodeImages
 */
class Basic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $name;
    public $parent_id;
    public $rang;
    public static function tableName()
    {
        return 'node';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['summary', 'description', 'other', 'text1', 'text2', 'text3', 'text4', 'text5'], 'string'],
            [['price', 'digit1', 'digit2', 'digit3', 'digit4', 'digit5'], 'number'],
            [['setting', 'started_at', 'ended_at', 'created_at', 'updated_at', 'created_by', 'updated_by', 'status', 'menu', 'base'], 'integer'],
            [['title', 'image', 'video', 'link', 'field1', 'field2', 'field3', 'inner', 'sub_title', 'type', 'slug','category','tags'], 'string', 'max' => 255],
            ['title','required'],
            [['name','parent_id','route','rang'],'safe'],
            [['name','parent_id','rang'],'required', 'when' => function ($model) {return $model->menu == 1;}, 'whenClient' => "function (attribute, value) { return $('#menu-checkbox').val() == 1; }"] 
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'summary' => 'Summary',
            'description' => 'Description',
            'other' => 'Other',
            'image' => 'Image',
            'video' => 'Video',
            'price' => 'Price',
            'link' => 'Link',
            'setting' => 'Setting',
            'digit1' => 'Digit1',
            'digit2' => 'Digit2',
            'digit3' => 'Digit3',
            'digit4' => 'Digit4',
            'digit5' => 'Digit5',
            'field1' => 'Field1',
            'field2' => 'Field2',
            'field3' => 'Field3',
            'inner' => 'Inner',
            'sub_title' => 'Sub_title',
            'text1' => 'Text1',
            'text2' => 'Text2',
            'text3' => 'Text3',
            'text4' => 'Text4',
            'text5' => 'Text5',
            'started_at' => 'Started At',
            'ended_at' => 'Ended At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'status' => 'Status',
            'menu' => 'Menu',
            'type' => 'Type',
            'slug' => 'Slug',
            'name' => 'Name',
            'parent_id' => 'Parent',
            'rang' => 'Weight',
        ];
    }
    public function behaviors()
    {
      return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
            ],
            [
                'class' => 'yii\behaviors\SluggableBehavior',
                'attribute' => 'title',
                'immutable' => true,
                'ensureUnique'=>true,
                // 'slugAttribute' => 'slug',
            ],
            [
                'class' => 'yii\behaviors\BlameableBehavior',
            ]
      ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNodeImages()
    {
        return $this->hasMany(NodeImage::className(), ['nid' => 'id']);
    }

    public function getCreatedBy()
    {
        return $this->hasOne('common\models\User', ['id' => 'created_by']);
    }
    public function getUpdatedBy()
    {
        return $this->hasOne('common\models\User', ['id' => 'updated_by']);
    }

    /**
     * Returns content and replace widgets short codes
     *
     * Widget short code example: [[\app\widgets\SomeWidget:method]]
     *
     * @return string
     */
    public function getContent()
    {
        $content = preg_replace_callback('/\[\[([^(\[\[)]+:[^(\[\[)]+)\]\]/is', [$this, 'replace'], $this->description);
        return $content;
    }
    /**
     * Replaces widget short code on appropriate widget
     *
     * @param $data
     *
     * @return string
     */
    private function replace($data)
    {
        $widget = explode(':', $data[1]);
        if (class_exists($class = $widget[0]) && method_exists($class, $method = $widget[1])) {
            return call_user_func([$class, $method]);
        }
        return '';
    }
   public function getMainTitle()
    {
        return preg_replace('/\s+/', ' ',$this->title.' '.$this->sub_title);
    }
}
