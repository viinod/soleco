<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "node_sub".
 *
 * @property int $id
 * @property int $nid
 * @property string $sub_title
 * @property int $icon
 * @property string $sub_description
 *
 * @property Node $n
 */
class NodeSub extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $img;
    public static function tableName()
    {
        return 'node_sub';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['nid'], 'integer'],
            [['description','summary','other','list'], 'string'],
            [['sub_title','image'], 'string', 'max' => 255],
            [['nid'], 'exist', 'skipOnError' => true, 'targetClass' => Node::className(), 'targetAttribute' => ['nid' => 'id']],
            ['img', 'image','maxWidth' => 800, 'maxHeight' => 600,'minWidth' => 400, 'minHeight' => 300,'extensions' => 'png, jpg'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nid' => 'Nid',
            'sub_title' => 'Sub Title',
            'icon' => 'Icon',
            'description' => 'Description',
            'other' => 'List 1',
            'list' => 'List 2',
        ];
    }
    public function getAvatarImage(){
        return yii\helpers\Html::img(Yii::getAlias('@url').'/advanced/'.$this->image);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getN()
    {
        return $this->hasOne(Node::className(), ['id' => 'nid']);
    }
}
