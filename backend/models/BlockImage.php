<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "image".
 *
 * @property int $id
 * @property int $b_id
 * @property string $title
 * @property string $alt
 * @property string $image
 *
 * @property Project $project
 */
class BlockImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $img;
    public static function tableName()
    {
        return 'block_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            ['img', 'required' ,'on' => 'create-only'],
            [['b_id'], 'integer'],
            [['title', 'alt', 'image'], 'string', 'max' => 255],
            [['b_id'], 'exist', 'skipOnError' => true, 'targetClass' => Block::className(), 'targetAttribute' => ['b_id' => 'id']],
            ['img', 'image', 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'b_id' => 'Block ID',
            'title' => 'Title',
            'alt' => 'Alt',
            'img' => 'Image',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'b_id']);
    }

    public function getAvatarImage(){
        return yii\helpers\Html::img(Yii::getAlias('@url').'/blockimages/'.$this->image);
    }
}
