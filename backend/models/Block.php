<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "block".
 *
 * @property int $id
 * @property string $title
 * @property string $category
 * @property string $description
 * @property string $sub_title
 * @property string $url
 * @property int $order
 * @property string $slug
 * @property int $status
 *
 * @property BlockImage[] $blockImages
 */
class Block extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $block_template;
    public static function tableName()
    {
        return 'block';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description', 'sub_title'], 'string'],
            [['order','icon','status'], 'integer'],
            [['title', 'category', 'url'], 'string', 'max' => 255],
            [['slug'], 'string', 'max' => 100],
            ['block_template','safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'category' => 'Category',
            'description' => 'Description',
            'sub_title' => 'Sub Title',
            'url' => 'Url',
            'order' => 'Order',
            'slug' => 'Slug',
            'status' => 'Status',
        ];
    }
    public function behaviors()
    {
      return [
            [
                'class' => 'yii\behaviors\SluggableBehavior',
                'attribute' => 'title',
                'ensureUnique'=>true,
                // 'slugAttribute' => 'slug',
            ],

            // 'sortable' => [
            // 'class' => \kotchuprik\sortable\behaviors\Sortable::className(),
            // 'query' => self::find(),
            // ],
      ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlockImages()
    {
        return $this->hasMany(BlockImage::className(), ['b_id' => 'id']);
    }
    public function getBlockImage()
    {
        return $this->hasOne(BlockImage::className(), ['b_id' => 'id']);
    }
   public function getMainTitle()
    {
        return preg_replace('/\s+/', ' ',$this->title.' '.$this->sub_title);
    }
}
