<?php

namespace backend\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "node_image".
 *
 * @property integer $id
 * @property integer $nid
 * @property string $node_image
 * @property string $node_image_title
 * @property string $node_image_description
 * @property integer $default
 *
 * @property Node $n
 */
class NodeImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'node_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['node_image_title'], 'required'],
            ['file', 'required' ,'on' => 'create-only'],
            [['nid', 'default'], 'integer'],
            [['node_image_description'], 'string'],
            [['node_image', 'node_image_title'], 'string', 'max' => 255],
            [['nid'], 'exist', 'skipOnError' => true, 'targetClass' => Node::className(), 'targetAttribute' => ['nid' => 'id']],
            [['date','text1','text2'],'safe'],
            ['text2','email','message' => 'Please enter valid email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nid' => 'Nid',
            'sister' => 'Image',
            'node_image_title' => 'Title',
            'node_image_description' => 'Description',
            'default' => 'Default',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getN()
    {
        return $this->hasOne(Node::className(), ['id' => 'nid']);
    }

    public static function psearch($id)
    {
        $query = Nodeimage::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->where(['nid'=>$id]);
         
          return $dataProvider;
    }
    public function getAvatarImage(){
        return yii\helpers\Html::img(Yii::getAlias('@url').'/nodeimages/'.$this->node_image);
    }
    public function getimageurl()
   {
      // return your image url here
      return Yii::getAlias('@url').'/nodeimages/'.$this->node_image;
   }
}
