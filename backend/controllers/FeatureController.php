<?php

namespace backend\controllers;

use Yii;
use backend\models\Block;
use backend\models\BlockImage;
use backend\models\Model;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

/**
 * FeatureController implements the CRUD actions for Block model.
 */
class FeatureController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
                        'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    // public function actions()
    // {
    //     return [
    //         'sorting' => [
    //             'class' => \kotchuprik\sortable\actions\Sorting::className(),
    //             'query' => Project::find(),
    //         ],
    //     ];
    // }

    /**
     * Lists all Block models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Block::find()->orderBy('order'),
        ]);
$dataProvider->query->where(['category' => 'feature'])->all();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Block model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionCreate()
    {
        $block = new Block();

        if ($block->load(Yii::$app->request->post())) {
            $block->category = 'feature';
            $block->save();
            $location = Yii::getAlias(Yii::$app->params['site.block']);
            $file =  $location.'block--id-'.$block->id.'.php'; 
            if(!file_exists($file))
            fopen($file, 'w');
            file_put_contents($file,$block->block_template);
            return $this->redirect(['view', 'id' => $block->id]);
        }

        return $this->render('create', [
            'block' => $block,
        ]);
    }

    /**
     * Updates an existing Block model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $block = $this->findModel($id);

        if ($block->load(Yii::$app->request->post())) {
            $block->save();
            //print_r($block->getErrors());exit;
            $location = Yii::getAlias(Yii::$app->params['site.block']);
            $file =  $location.'block--id-'.$block->id.'.php'; 
            if(!file_exists($file))
            fopen($file, 'w');
            file_put_contents($file,$block->block_template);
            return $this->redirect(['view', 'id' => $block->id]);
        }

        return $this->render('update', [
            'block' => $block,
        ]);
    }
  

    /**
     * Deletes an existing Block model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Block model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Block the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Block::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
