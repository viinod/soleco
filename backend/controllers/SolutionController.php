<?php

namespace backend\controllers;

use Yii;
use backend\models\Solution;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use backend\models\Model;
use backend\models\NodeImage;
use backend\models\NodeSub;
use yii\helpers\ArrayHelper;
use common\helpers\Cms;
/**
 * SolutionController implements the CRUD actions for Solution model.
 */
class SolutionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
                        'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Lists all Solution models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Solution::find()->where(['type' => 'solution']),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Solution model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Solution model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $model = new Solution();
        if ($model->load(Yii::$app->request->post())) {
            $model->type = 'solution';
            $model->base = 1; 
            $model->img = UploadedFile::getInstance($model,'img');
            if($model->img != NULL){
            $model->image = Cms::clean($model->img->baseName).'-'.time().'.'.$model->img->extension;
            }
            $model->img1 = UploadedFile::getInstance($model,'img1');
            if($model->img1 != NULL){
            $model->file = Cms::clean($model->img1->baseName).'-'.time().'.'.$model->img1->extension;
            }
            $model->save();
            if($model->img != NULL){
                    $model->img->saveAs(Yii::getAlias('@path').'/solutions/'.$model->image); 
            }
            if($model->img1 != NULL){
                    $model->img1->saveAs(Yii::getAlias('@path').'/solutions/'.$model->file); 
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->img = UploadedFile::getInstance($model,'img');
            if($model->img != NULL){
            $model->image = Cms::clean($model->img->baseName).'-'.time().'.'.$model->img->extension;
            }
            $model->img1 = UploadedFile::getInstance($model,'img1');
            if($model->img1 != NULL){
            $model->file = Cms::clean($model->img1->baseName).'-'.time().'.'.$model->img1->extension;
            }
            $model->save();
            if($model->img != NULL){
                    $model->img->saveAs(Yii::getAlias('@path').'/solutions/'.$model->image); 
            }
            if($model->img1 != NULL){
                    $model->img1->saveAs(Yii::getAlias('@path').'/solutions/'.$model->file); 
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Solution model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }
    
    /**
     * Finds the Solution model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Solution the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Solution::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
