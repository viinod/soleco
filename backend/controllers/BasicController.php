<?php

namespace backend\controllers;

use Yii;
use backend\models\Basic;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\NodeImage;
use yii\web\UploadedFile;
use common\helpers\Cms;

/**
 * BasicController implements the CRUD actions for Basic model.
 */
class BasicController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
                        'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Basic models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Basic::find()->where(['type' => 'basic']),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Basic model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        //  $user = Basic::findOne($id);
        //  $user->created_at = date("m-d-Y",$user->created_at);
        //  return $user;
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Basic model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Basic();
        if ($model->load(Yii::$app->request->post()) ) {
            $model->type = 'basic';
            $model->base = 1;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Updates an existing Basic model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->type = !empty($model->type) ? $model->type : "basic";
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Basic model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }
    public function actionSee($id,$nid)
    {
        $model = NodeImage::findOne(['id' => $id,'nid' => $nid]);
        return $this->render('see', [
            'model' => $model,
            'node' => $this->findModel($nid),
        ]);
    }
    public function actionThrash($nid,$id)
    {
        if (($model = NodeImage::findOne(['id' => $id,'nid' => $nid])) !== null) {
            $model->delete();
        }
        return $this->redirect(['list', 'nid' => $nid]);
    }
    public function actionList($nid)
    {

        $dataProvider = new ActiveDataProvider([
            'query' => NodeImage::find(),
        ]);
        $dataProvider->query->where(['nid' => $nid])->all();
        return $this->render('list', [
            'dataProvider' => $dataProvider,
            'node' => $this->findModel($nid),
        ]);
    }
    public function actionAdd($nid)
    {
        $model = new NodeImage();
        if ($model->load(Yii::$app->request->post())) {
            $model->nid = $nid;
            $model->date = !empty($model->date) ? strtotime($model->date) : '';
            $model->sister = UploadedFile::getInstance($model,'sister');
            $model->brand = UploadedFile::getInstance($model,'brand');
            $model->offer = UploadedFile::getInstance($model,'offer');
            if($model->sister != NULL){
            $model->node_image = Cms::clean($model->sister->baseName).'-'.time().'.'.$model->sister->extension;
            }
            if($model->brand != NULL){
            $model->node_image = Cms::clean($model->brand->baseName).'-'.time().'.'.$model->brand->extension;
            }
            if($model->offer != NULL){
            $model->node_image = Cms::clean($model->offer->baseName).'-'.time().'.'.$model->offer->extension;
            }
            $model->save();
            if($model->sister != NULL){
                    $model->sister->saveAs(Yii::getAlias('@path').'/nodeimages/'.$model->node_image); 
            }
            if($model->brand != NULL){
                    $model->brand->saveAs(Yii::getAlias('@path').'/nodeimages/'.$model->node_image); 
            }
            if($model->offer != NULL){
                    $model->offer->saveAs(Yii::getAlias('@path').'/nodeimages/'.$model->node_image); 
            }
            return $this->redirect(['see', 'id' => $model->id,'nid' => $nid]);
        }
        return $this->render('add', [
            'model' => $model,
            'node' => $this->findModel($nid),
        ]);
    }
     public function actionEdit($nid,$id)
    {
        $model = NodeImage::findOne(['id' => $id,'nid' => $nid]);
        if ($model->load(Yii::$app->request->post())) {
            $model->nid = $nid;
            $model->date = !empty($model->date) ? strtotime($model->date) : '';
            $model->sister = UploadedFile::getInstance($model,'sister');
            $model->brand = UploadedFile::getInstance($model,'brand');
            $model->offer = UploadedFile::getInstance($model,'offer');
            if($model->sister != NULL){
            $model->node_image = Cms::clean($model->sister->baseName).'-'.time().'.'.$model->sister->extension;
            }
            if($model->brand != NULL){
            $model->node_image = Cms::clean($model->brand->baseName).'-'.time().'.'.$model->brand->extension;
            }
            if($model->offer != NULL){
            $model->node_image = Cms::clean($model->offer->baseName).'-'.time().'.'.$model->offer->extension;
            }
            $model->save();
            if($model->sister != NULL){
                    $model->sister->saveAs(Yii::getAlias('@path').'/nodeimages/'.$model->node_image); 
            }
            if($model->brand != NULL){
                    $model->brand->saveAs(Yii::getAlias('@path').'/nodeimages/'.$model->node_image); 
            }
            if($model->offer != NULL){
                    $model->offer->saveAs(Yii::getAlias('@path').'/nodeimages/'.$model->node_image); 
            }
            return $this->redirect(['see', 'id' => $model->id,'nid' => $nid]);
        }
        return $this->render('edit', [
            'model' => $model,
            'node' => $this->findModel($nid),
        ]);
    }
    /**
     * Finds the Basic model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Basic the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Basic::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
