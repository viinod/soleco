<?php

namespace backend\controllers;

use Yii;
use backend\models\Template;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TemplateController implements the CRUD actions for Template model.
 */
class TemplateController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
                        'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Template models.
     * @return mixed
     */
    // public function actionIndex()
    // {
    //     $dataProvider = new ActiveDataProvider([
    //         'query' => Template::find(),
    //     ]);

    //     return $this->render('index', [
    //         'dataProvider' => $dataProvider,
    //     ]);
    // }

    /**
     * Displays a single Template model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Template model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionIndex()
    {
        $model = new Template();
        $model->scenario = 'file-create';
        if ($model->load(Yii::$app->request->post())) {
            if($model->category == 'template'){
             $location = Yii::getAlias(Yii::$app->params['site.template']);
             $file = (strpos($model->name,'.php')) ? $location.str_replace(' ', '-',$model->name) : $location.str_replace(' ', '-',$model->name).'.php';   
            }elseif($model->category == 'layout'){
             $location = Yii::getAlias(Yii::$app->params['site.layout']);
             $file = (strpos($model->name,'.php')) ? $location.str_replace(' ', '-',$model->name) : $location.str_replace(' ', '-',$model->name).'.php';   
            }elseif($model->category == 'widget'){
             $location = Yii::getAlias(Yii::$app->params['site.widget']);
             $file = (strpos($model->name,'.php')) ? $location.str_replace(' ', '-',$model->name) : $location.str_replace(' ', '-',$model->name).'.php';   
            }elseif($model->category == 'css'){
             $location = Yii::getAlias(Yii::$app->params['site.css']);
             $file = (strpos($model->name,'.css')) ? $location.str_replace(' ', '-',$model->name) : $location.str_replace(' ', '-',$model->name).'.css';   
            }else{
             $location = Yii::getAlias(Yii::$app->params['site.js']);
             $file = (strpos($model->name,'.js')) ? $location.str_replace(' ', '-',$model->name) : $location.str_replace(' ', '-',$model->name).'.js';   
            }
            
            if(!file_exists($file))
            fopen($file, 'w');            
            return $this->redirect(['index']);
        } else {
            return $this->render('file', [
                'model' => $model,
            ]);
        }
    }

    public function actionPhp($file = '')
    {
        
        $templates = array();
        $template_folder = Yii::getAlias(Yii::$app->params['site.template']);
        if(file_exists($template_folder))
        $templates = \yii\helpers\FileHelper::findFiles($template_folder,['only' => ['*.php']]);
        $templates = str_replace($template_folder, "", $templates);
        $templates = str_replace('\\', "", $templates);
        $file = !empty($file) ? $file : reset($templates);
        if(file_exists($template_folder.$file)){
            $fcontent = file_get_contents($template_folder.$file);
            if (Yii::$app->request->post()) {
                if(file_exists($template_folder.$file))
                    file_put_contents($template_folder.$file,Yii::$app->request->post('content'));
                return $this->redirect(['php', 'file' => $file]);
            }else{

            return $this->render('php', [
            'file' => $file,
            'fcontent' => $fcontent,
            'templates' => $templates,
        ]);
            }
        }else{
            return $this->redirect(['index']);
        }
    }
    public function actionCss($file = '')
    {
        
        $templates = array();
        $template_folder = Yii::getAlias(Yii::$app->params['site.css']);
        if(file_exists($template_folder))
        $templates = \yii\helpers\FileHelper::findFiles($template_folder,['only' => ['*.css']]);
        $templates = str_replace($template_folder, "", $templates);
        $templates = str_replace('\\', "", $templates);
        $file = !empty($file) ? $file : reset($templates);
        if(file_exists($template_folder.$file)){
            $fcontent = file_get_contents($template_folder.$file);
            if (Yii::$app->request->post()) {
                if(file_exists($template_folder.$file))
                    file_put_contents($template_folder.$file,Yii::$app->request->post('content'));
                return $this->redirect(['css', 'file' => $file]);
            }else{

            return $this->render('css', [
            'file' => $file,
            'fcontent' => $fcontent,
            'templates' => $templates,
        ]);
            }
        }else{
            return $this->redirect(['index']);
        }
    }
    public function actionJs($file = '')
    {
        
        $templates = array();
        $template_folder = Yii::getAlias(Yii::$app->params['site.js']);
        if(file_exists($template_folder))
        $templates = \yii\helpers\FileHelper::findFiles($template_folder,['only' => ['*.js']]);
        $templates = str_replace($template_folder, "", $templates);
        $templates = str_replace('\\', "", $templates);
        $file = !empty($file) ? $file : reset($templates);
        if(file_exists($template_folder.$file)){
            $fcontent = file_get_contents($template_folder.$file);
            if (Yii::$app->request->post()) {
                if(file_exists($template_folder.$file))
                    file_put_contents($template_folder.$file,Yii::$app->request->post('content'));
                return $this->redirect(['js', 'file' => $file]);
            }else{

            return $this->render('js', [
            'file' => $file,
            'fcontent' => $fcontent,
            'templates' => $templates,
        ]);
            }
        }else{
            return $this->redirect(['index']);
        }
    }
    public function actionLayout($file = '')
    {
        
        $templates = array();
        $template_folder = Yii::getAlias(Yii::$app->params['site.layout']);
        if(file_exists($template_folder))
        $templates = \yii\helpers\FileHelper::findFiles($template_folder,['only' => ['*.php']]);
        $templates = str_replace($template_folder, "", $templates);
        $templates = str_replace('\\', "", $templates);
        $file = !empty($file) ? $file : reset($templates);
        if(file_exists($template_folder.$file)){
            $fcontent = file_get_contents($template_folder.$file);
            if (Yii::$app->request->post()) {
                if(file_exists($template_folder.$file))
                    file_put_contents($template_folder.$file,Yii::$app->request->post('content'));
                return $this->redirect(['layout', 'file' => $file]);
            }else{

            return $this->render('layout', [
            'file' => $file,
            'fcontent' => $fcontent,
            'templates' => $templates,
        ]);
            }
        }else{
            return $this->redirect(['index']);
        }
    }
    public function actionWidget($file = '')
    {
        
        $templates = array();
        $template_folder = Yii::getAlias(Yii::$app->params['site.widget']);
        if(file_exists($template_folder))
        $templates = \yii\helpers\FileHelper::findFiles($template_folder,['only' => ['*.php']]);
        $templates = str_replace($template_folder, "", $templates);
        $templates = str_replace('\\', "", $templates);
        $file = !empty($file) ? $file : reset($templates);
        if(file_exists($template_folder.$file)){
            $fcontent = file_get_contents($template_folder.$file);
            if (Yii::$app->request->post()) {
                if(file_exists($template_folder.$file))
                    file_put_contents($template_folder.$file,Yii::$app->request->post('content'));
                return $this->redirect(['widget', 'file' => $file]);
            }else{

            return $this->render('widget', [
            'file' => $file,
            'fcontent' => $fcontent,
            'templates' => $templates,
        ]);
            }
        }else{
            return $this->redirect(['index']);
        }
    }       
    /**
     * Updates an existing Template model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'assigning-template';
        $templates = array();
        $file = Yii::getAlias(Yii::$app->params['site.template']);
        if(file_exists($file))
        $templates = \yii\helpers\FileHelper::findFiles($file,['only' => ['*.php']]);
        $templates = str_replace($file, "", $templates);
        $templates = str_replace('\\', "", $templates);
        $templates = array_combine($templates, $templates);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'templates' => $templates,
            ]);
        }
    }

    /**
     * Deletes an existing Template model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Template model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Template the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Template::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
