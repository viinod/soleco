<?php

namespace backend\controllers;

use Yii;
use backend\models\Advanced;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use backend\models\Model;
use backend\models\NodeImage;
use backend\models\NodeSub;
use yii\helpers\ArrayHelper;
use common\helpers\Cms;
/**
 * AdvancedController implements the CRUD actions for Advanced model.
 */
class AdvancedController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
                        'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Lists all Advanced models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Advanced::find()->where(['type' => 'advanced']),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Advanced model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionSee($id,$nid)
    {
        $model = NodeSub::findOne(['id' => $id,'nid' => $nid]);
        return $this->render('see', [
            'model' => $model,
            'node' => $this->findModel($nid),
        ]);
    }
    /**
     * Creates a new Advanced model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $model = new Advanced();
        if ($model->load(Yii::$app->request->post())) {
            $model->type = 'advanced';
            $model->img = UploadedFile::getInstance($model,'img');
            if($model->img != NULL){
            $model->image = Cms::clean($model->img->baseName).'-'.time().'.'.$model->img->extension;
            }
            $model->save();
            if($model->img != NULL){
                    $model->img->saveAs(Yii::getAlias('@path').'/advanced/'.$model->image); 
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->img = UploadedFile::getInstance($model,'img');
            if($model->img != NULL){
            $model->image = Cms::clean($model->img->baseName).'-'.time().'.'.$model->img->extension;
            }
            $model->save();
            if($model->img != NULL){
                    $model->img->saveAs(Yii::getAlias('@path').'/advanced/'.$model->image); 
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Advanced model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }
    public function actionThrash($nid,$id)
    {
        if (($model = NodeSub::findOne(['id' => $id,'nid' => $nid])) !== null) {
            $model->delete();
        }
        return $this->redirect(['list', 'nid' => $nid]);
    }
    public function actionList($nid)
    {

        $dataProvider = new ActiveDataProvider([
            'query' => NodeSub::find(),
        ]);
        $dataProvider->query->where(['nid' => $nid])->all();
        return $this->render('list', [
            'dataProvider' => $dataProvider,
            'node' => $this->findModel($nid),
        ]);
    }
    public function actionAdd($nid)
    {
        $model = new NodeSub();
        if ($model->load(Yii::$app->request->post())) {
            $model->img = UploadedFile::getInstance($model,'img');
            $model->nid = $nid;
            if($model->img != NULL){
            $model->image = Cms::clean($model->img->baseName).'-'.time().'.'.$model->img->extension;
            }
            $model->save();
            if($model->img != NULL){
                    $model->img->saveAs(Yii::getAlias('@path').'/advanced/'.$model->image); 
            }
            return $this->redirect(['see', 'id' => $model->id,'nid' => $nid]);
        }
        return $this->render('add', [
            'model' => $model,
            'node' => $this->findModel($nid),
        ]);
    }
     public function actionEdit($nid,$id)
    {
        $model = NodeSub::findOne(['id' => $id,'nid' => $nid]);
        if ($model->load(Yii::$app->request->post())) {
            $model->img = UploadedFile::getInstance($model,'img');
            $model->nid = $nid;
            if($model->img != NULL){
            $model->image = Cms::clean($model->img->baseName).'-'.time().'.'.$model->img->extension;
            }
            $model->save();
            if($model->img != NULL){
                    $model->img->saveAs(Yii::getAlias('@path').'/advanced/'.$model->image); 
            }
            return $this->redirect(['see', 'id' => $model->id,'nid' => $nid]);
        }
        return $this->render('edit', [
            'model' => $model,
            'node' => $this->findModel($nid),
        ]);
    }
    /**
     * Finds the Advanced model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Advanced the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Advanced::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
