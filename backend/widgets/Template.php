<?php

namespace backend\widgets;

use Yii;
use yii\base\Widget;

class Template extends Widget
{         
public $category;
public $type;
public $folder;
        public function init()
        {
        	parent::init();
            $this->folder = Yii::getAlias(Yii::$app->params['site.template']);
        }
        public function run()
        {
            if($this->category == 'list'){
                if(!empty($this->type) && file_exists($this->folder.'/page--list--'.$this->type.'.php')){
                    return 'page--list--'.$this->type.'.php';
                }elseif(file_exists($this->folder.'/page--list.php')){
                    return 'page--list.php';
                }else{
                    return;
                }

            }else{
                if(!empty($this->type) && file_exists($this->folder.'/page--node--'.$this->type->id.'.php')){
                    return 'page--node--'.$this->type->id.'.php';
                }elseif(!empty($this->type) && file_exists($this->folder.'/page--node--'.$this->type->slug.'.php')){
                    return 'page--node--'.$this->type->slug.'.php';
                }elseif(!empty($this->type) && file_exists($this->folder.'/page--node--'.$this->type->type.'.php')){
                    return 'page--node--'.$this->type->type.'.php';
                }elseif(file_exists($this->folder.'/page--node.php')){
                    return 'page--node.php';
                }else{
                    return;
                }
            }

        }
}
?>
