-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 08, 2020 at 04:58 PM
-- Server version: 5.7.9
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `soleco`
--

-- --------------------------------------------------------

--
-- Table structure for table `block`
--

DROP TABLE IF EXISTS `block`;
CREATE TABLE IF NOT EXISTS `block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `category` varchar(255) DEFAULT 'block',
  `description` text,
  `sub_title` text,
  `url` varchar(255) DEFAULT NULL,
  `icon` smallint(6) DEFAULT NULL,
  `order` smallint(11) DEFAULT NULL,
  `slug` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `block`
--

INSERT INTO `block` (`id`, `title`, `category`, `description`, `sub_title`, `url`, `icon`, `order`, `slug`, `status`) VALUES
(1, 'Going Solar', NULL, 'To Go Solar has never before been easier. Our team of independent solar consultants will help you sort through your options. To see how much you can save, and for all other solar questions, request a free consultation. There’s no obligation.', 'is Easy with Soleco', '#', NULL, NULL, 'going-solar-2', 1),
(3, 'Advantages of ', 'block', 'SolEco, a Solar renewal energy company who provides On Grid, Off Grid and Hybrid solutions to our customers in various segments cost effectively without compromising on the quality of the products and services. Our consultants can create customized solutions that best suits the environment &amp; need of our customers.', 'Solar Energy', '#', NULL, NULL, 'advantages-of', 1),
(4, 'OUR', 'block', 'We realize the fact that, people throughout the world are facing climate problems and must address the limited availability of the current energy sources. Solar energy is the way of the future and we SOLECO provide tailor made solar energy solutions to our valued customers in residential and commercial sector.', 'PORTFOLIO', '#', NULL, NULL, 'our', 1),
(5, 'Residential', 'block', 'Experience the benefits of solar power for your home. Residential solar energy is clean, efficient and protect home owners from rising electricity bills. Net metering allows home owners to sell excess electricity produced by solar energy back to the utility company.', 'Solar Solutions', '#', NULL, NULL, 'residential', 1),
(6, 'Commercial', 'block', 'Solar power is the way of the future and it can effectively reduce long term energy costs for small and large commercial / Enterprise properties. Solar energy adaption is apt for the commercial industry because the consumption of electricity is a lot more than the consumption of residences. The power tariff rates are also much higher compared to the residential.', 'Solar Solutions', '#', NULL, NULL, 'commercial', 1),
(7, 'WHY', 'block', 'Soleco Energy Private Ltd is dedicated to bringing renewable energy to the region to helping the environment, to establishing energy independence and to educating the public on how to take part in making a healthier more secure future. We just not bringing solar, but our goal is to have a long-term partnership with our customers in providing their energy needs in the most cost effective and efficient way possible.', 'SOLECO', '#', NULL, NULL, 'why', 1),
(8, 'Consults', 'feature', 'We start every project with a consultative approach that meets your needs and goals for a solar plant. A site visit is conducted by a trained consultant to learn about and to analyze your property as well as to evaluate your energy requirements. Based on the information a gathered a customized techno commercial proposal is created that will detail all your solar energy options.', NULL, NULL, 3, NULL, 'consults', 1),
(9, 'Design', 'feature', 'Our solar expert will construct a detailed design of the site along with a simulation detailing your possible energy savings. All designs are scalable, keeping in mind you can start small and expand the system over a period. Based on your opinion of what is contained in the design, the solar expert will modify the agreement as necessary so that it is exactly to your specifications and the final agreement is signed off.', NULL, NULL, 4, NULL, 'design', 1),
(10, 'Permit', 'feature', 'After you approve the finalized agreement, then we legally required to submit your personalized plan to the concerned local authorities for permitting approval. The local utility provider gives the feasibility report and permission to move forward with solar panel installation and commissioning.', NULL, NULL, 5, NULL, 'permit', 1),
(11, 'Build', 'feature', 'he build phase consists of various activities starting from delivery of materials, placing of rails, install wiring, place PV panels, inspection, grid connectivity and power ON. Our well experienced implementation team takes at most care at each activities of the build phase to ensure the environment meets the specifications defined in the design document. Necessary procedure and guidelines will be provided for a phased rollout approach to mitigate risk and ensure successful deployment.', NULL, NULL, 6, NULL, 'build', 1),
(12, 'Optimize', 'feature', 'Analyze and improve the existing environment and transfer best practices knowledge. Conduct periodic proactive health checks of the system and provide with concrete & actionable recommendations. With onsite delivery capabilities, our Professional Services team can provide 24x7 support to mission critical environments and keep the customer environment stable.', NULL, NULL, 7, NULL, 'optimize', 1),
(13, 'Save the', 'feature', 'Go a long way in lowering your carbon footprint by replacing utility power with clean electricity from solar panels. People choose to go solar because it makes good sense from a financial point of view, but the environmental benefits are clearly worth highlighting. “Investing in today’s and tomorrow’s generations! “ ', 'Environment', '#', 10, NULL, 'save-the', 1),
(14, 'Cut off your energy bills', 'feature', 'With the introduction of net metering and feed-in tariff (FIT) schemes, customers can now “sell” excess electricity, or receive bill credits, during times when they produce more electricity than what they actually consume.\"This means that homeowners & commercial business can reduce their overall electricity expenses by going solar\"', 'Create Revenue', '#', 11, NULL, 'cut-off-your-energy-bills', 1),
(15, 'Control your', 'feature', 'The wide array of residential / commercial solar energy offerings allows users the ability to take control of their energy use and storage in a more effective and energy-efficient manner. You have the freedom to generate, store, feed and you are energy independent.', 'Energy', '#', 13, NULL, 'control-your', 1),
(16, 'Low', 'feature', 'You can expect very little spending on maintenance and repair work of Solar plants. The Solar panel manufacturers offer 25 years warranty and the inverters needs to be changed after 5-10 years only. There are no moving parts in the solution hence no wear and tear and the only maintenance require is to keep the panels clean as much as possible', 'Maintenance Cost', '#', 14, NULL, 'low', 1),
(17, 'Our Experience', 'feature', 'The wide array of residential / commercial solar energy offerings allows users the ability to take control of their energy use and storage in a more effective and energy-efficient manner. You have the freedom to generate, store, feed and you are energy independent.', '', '#', 8, NULL, 'our-experience', 1),
(18, 'Consultative Approach', 'feature', 'We believe in building a relationship with our customer contact and identify solutions to their challenges through open-ended questions and active listening. We always put the customer’s needs over our interests.', '', '#', 3, NULL, 'consultative-approach', 1),
(19, 'Project Execution', 'feature', 'We strictly follow the industry best practices when it comes to project delivery & execution. Our trained, certified and experienced personnel will complete each milestone of the project adhering to the standards.', '', '#', 14, NULL, 'project-execution', 1),
(20, 'Operational Support', 'feature', 'Operational support is key to get the best out of any project.  Our 24 / 7 support team can provide periodic services and maintenance activities which will ensure your system is fully functional in the best efficient manner.', '', '#', 6, NULL, 'operational-support', 1),
(21, 'Cost Effective', 'feature', 'We associate with multiple technology partners to bring in the best price across to our customers without compromising the value, benefits and features.', '', '#', 11, NULL, 'cost-effective', 1),
(22, 'Top Quality', 'feature', 'We realize quality is critical to satisfying our customers and retaining their loyalty. Keeping this in mind we go an extra step to provide excellent service quality and best leading products that meets the objectives of our customers.', '', '#', 9, NULL, 'top-quality', 1),
(23, 'Regulations / compliances ', 'feature', 'All our turn-key implementations strictly follow the legal / technical compliances and regulations set by the authorities. We educate and provide assistance in paper works to our customers to fulfill the needs of the local government.', '', '#', 5, NULL, 'regulations-compliances', 1);

-- --------------------------------------------------------

--
-- Table structure for table `block_image`
--

DROP TABLE IF EXISTS `block_image`;
CREATE TABLE IF NOT EXISTS `block_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `b_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`b_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `title`) VALUES
(1, 'Exterior'),
(2, 'Interior'),
(3, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `imagemanager`
--

DROP TABLE IF EXISTS `imagemanager`;
CREATE TABLE IF NOT EXISTS `imagemanager` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fileName` varchar(128) NOT NULL,
  `fileHash` varchar(32) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `createdBy` int(10) UNSIGNED DEFAULT NULL,
  `modifiedBy` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `imagemanager`
--

INSERT INTO `imagemanager` (`id`, `fileName`, `fileHash`, `created`, `modified`, `createdBy`, `modifiedBy`) VALUES
(3, 'grad-icon1.png', 'G1l_GoL_eOaiUMF5M08xV7KsVQjPxxJt', '2019-11-04 12:47:37', '2019-11-04 12:47:37', NULL, NULL),
(4, 'grad-icon2.png', 'fwtLGsoW-t1zBd-lgex5JAP9R7yqo4iL', '2019-11-04 12:47:37', '2019-11-04 12:47:37', NULL, NULL),
(5, 'grad-icon3.png', 'b35pEFGFRGJgR-NUlXf52WU2UduN_ZQ1', '2019-11-04 12:47:37', '2019-11-04 12:47:37', NULL, NULL),
(6, 'grad-icon4.png', 'ZiAx6zkWiUFf15ySGTEAabG-GfcIikJq', '2019-11-04 12:47:37', '2019-11-04 12:47:37', NULL, NULL),
(7, 'grad-icon5.png', '5Mf_lCy94YNeTQeED_a77x42TLjTkeXq', '2019-11-04 12:47:37', '2019-11-04 12:47:37', NULL, NULL),
(8, 'grad-icon6.png', 'YJFhqCTeIaQdIcRpfxbX1SO4CH39tKMB', '2019-11-04 12:47:37', '2019-11-04 12:47:37', NULL, NULL),
(9, 'grad-icon7.png', 'r2B4ay8f5cKK2ay_dkD1ObZtVYeFTXso', '2019-11-04 12:47:37', '2019-11-04 12:47:37', NULL, NULL),
(10, 'service-icon1.png', 'NYttYWCbqC0OVREVSicXMFpdiVH03GyY', '2019-11-04 12:47:37', '2019-11-04 12:47:37', NULL, NULL),
(11, 'service-icon2.png', 'hcbbhdgDMBHLo9u6VUBFAFjXOZQkT6vr', '2019-11-04 12:47:37', '2019-11-04 12:47:37', NULL, NULL),
(12, 'service-icon3.png', '2-z4LU5gr3FAEFvOX1r2BvS4xn3Mi0K2', '2019-11-04 12:47:37', '2019-11-04 12:47:37', NULL, NULL),
(13, 'service-icon4.png', 'vaHbrLRAts63WV9qa2xwXOJfc62Rhgj2', '2019-11-04 12:47:37', '2019-11-04 12:47:37', NULL, NULL),
(14, 'service-icon5.png', '0SYVPAiBrCpGa-_FvmlFUaShfjGq2oy9', '2019-11-04 12:47:37', '2019-11-04 12:47:37', NULL, NULL),
(15, 'spec-icon1.png', 'fWajVoM3_knh85gi_6zQalJyiy2GhcFt', '2019-11-04 12:47:37', '2019-11-04 12:47:37', NULL, NULL),
(16, 'logo.png', 'UfUwBTTrFhVx8-rTaFAnQa-CrurU4NaO', '2019-11-05 16:00:23', '2019-11-05 16:00:23', NULL, NULL),
(17, 'welcome.jpg', 'qIC072MtxAwSogxdaEY-7FH4rnhSr0Ta', '2019-11-05 17:53:24', '2019-11-05 17:53:24', NULL, NULL),
(18, 'ecology.jpg', 'QdVWaIaoAt40Kb4DM7JklzrF4P9_o5I3', '2019-11-05 17:53:46', '2019-11-05 17:53:46', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `translation` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`,`language`),
  KEY `idx_message_language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `language`, `translation`) VALUES
(1, 'en-US', '© {0,date,Y} Copyright Soleco. All Rights Reserved.'),
(2, 'en-US', 'SolEco is a renewable energy company active in designing and implementing alternative energy solutions. With expertise in both grid-connect and off-grid power, we are committed to providing flexible and clean energy solutions to fit all needs...'),
(3, 'en-US', 'There was an error sending your message.'),
(4, 'en-US', 'Thank you for contacting us. We will respond to you as soon as possible.'),
(5, 'en-US', 'To Apply on this position, mention the <strong>POST</strong> and send your resume to'),
(6, 'en-US', 'CAN\'T FIND YOUR JOB'),
(7, 'en-US', 'We appreciate your interest in exploring career opportunities with us. We are looking for talented professionals to join our team. Feel free to share your CV');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1564485438),
('m130524_201442_init', 1564485446),
('m190124_110200_add_verification_token_column_to_user_table', 1564485447),
('m150207_210500_i18n_init', 1564818055),
('m160622_085710_create_ImageManager_table', 1565096103),
('m170223_113221_addBlameableBehavior', 1565096103);

-- --------------------------------------------------------

--
-- Table structure for table `node`
--

DROP TABLE IF EXISTS `node`;
CREATE TABLE IF NOT EXISTS `node` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `summary` text,
  `description` text,
  `other` text,
  `image` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `setting` tinyint(1) DEFAULT '0',
  `category` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `digit1` decimal(10,2) DEFAULT NULL,
  `digit2` decimal(10,2) DEFAULT NULL,
  `digit3` decimal(10,2) DEFAULT NULL,
  `digit4` decimal(10,2) DEFAULT NULL,
  `digit5` decimal(10,2) DEFAULT NULL,
  `field1` varchar(255) DEFAULT NULL,
  `field2` varchar(255) DEFAULT NULL,
  `field3` varchar(255) DEFAULT NULL,
  `inner` varchar(255) DEFAULT NULL,
  `sub_title` varchar(255) DEFAULT NULL,
  `text1` text,
  `text2` text,
  `text3` text,
  `text4` text,
  `text5` text,
  `started_at` int(11) DEFAULT NULL,
  `ended_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `base` tinyint(1) DEFAULT '1',
  `menu` tinyint(4) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `node`
--

INSERT INTO `node` (`id`, `title`, `summary`, `description`, `other`, `image`, `video`, `price`, `link`, `file`, `setting`, `category`, `tags`, `digit1`, `digit2`, `digit3`, `digit4`, `digit5`, `field1`, `field2`, `field3`, `inner`, `sub_title`, `text1`, `text2`, `text3`, `text4`, `text5`, `started_at`, `ended_at`, `created_at`, `updated_at`, `created_by`, `updated_by`, `status`, `base`, `menu`, `type`, `slug`) VALUES
(1, 'Banner1', NULL, NULL, NULL, 'Banner1-1572864372.jpg', NULL, NULL, '#', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1572864372, 1572864372, 1, 1, 1, 1, NULL, 'banner', 'banner1'),
(2, 'Banner2', NULL, NULL, NULL, 'Banner2-1572864401.jpg', NULL, NULL, '#', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1572864401, 1572864401, 1, 1, 1, 1, NULL, 'banner', 'banner2'),
(3, 'Banner3', NULL, NULL, NULL, 'Banner3-1572864423.jpg', NULL, NULL, '#', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1572864423, 1572864423, 1, 1, 1, 1, NULL, 'banner', 'banner3'),
(4, 'Client Name', NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', NULL, 'Client-Name-1572866483.jpg', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Designation', 'Company', NULL, NULL, NULL, NULL, NULL, 1572866483, 1572866483, 1, 1, 1, 1, NULL, 'testimonial', 'client-name'),
(5, 'Solartek', NULL, NULL, NULL, 'Solartek-1572938232.jpg', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1572937744, 1572938232, 1, 1, 1, 1, NULL, 'partner', 'solartek'),
(6, 'Ac Expert', NULL, NULL, NULL, 'Ac-Expert-1572938245.jpg', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1572937843, 1572938245, 1, 1, 1, 1, NULL, 'partner', 'ac-expert'),
(7, 'About', 'Providing power and jobs for the community', '<p>SOLECO Energy Private Limited is a subsidiary company of Integraltech Networks LLC, Dubai <a href=\"http://www.integralnetworks.ae/\" target=\"_blank\" rel=\"noopener\">www.integralnetworks.ae</a>, with its registered office in Kochi, India. With rich experience internationally, company has ventured in India in the field of renewable energy. Within a short span of time we have established a service delivery arm at Kochi and started operation across India. We specialize in providing renewable energy solutions and services to our customers in Residential, Commercial and Agricultural segments in the region. We offer turn key solar energy solutions by adopting industry best practices &amp; methodologies in energy efficiency consulting, designing, implementation and maintenance.</p>', '', 'About-1578501111.jpg', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'About-1578501310.jpg', 'Soleco', NULL, NULL, NULL, NULL, 'Soleco', NULL, NULL, 1573126137, 1578501310, 1, 1, NULL, 1, NULL, 'advanced', 'about-us'),
(8, 'Contact Us', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Contact-Us-1578501469.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1577629840, 1578501469, 1, 1, NULL, 1, NULL, 'basic', 'contact-us'),
(9, 'Our', 'We create the best Sustainable design with nature in mind', '<p>SOLECO, a Solar renewal energy company who provides On Grid, Off Grid and Hybrid solutions to our customers in various segments cost effectively without compromising on the quality of the products and services. Our consultants can create customized solutions that best suits the environment &amp; need of our customers.</p>', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Solutions', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1578029034, 1578029034, 1, 1, NULL, 1, NULL, 'basic', 'soleco-solutions'),
(10, 'On-Grid or Grid Tied ', 'An On-Grid or Grid -Tied solar panel system is simply a solar energy system that is connected to the local utility grid. ', '<p>An On-Grid or Grid -Tied solar panel system is simply a solar energy system that is connected to the local utility grid. The On -Grid solar systems are suitable for residential and commercial use depending on the load requirements. These systems send excess power generated by the solar power system to the utility and consumers get compensated for the extra power fed back. When there is not enough sunlight to meet your business needs the system runs on the power supplied by the grid. These systems are best suitable when your power consumption is high, and you wish to reduce your electricity bills. The photovoltaic (PV) solar panels absorbs sunlight as a source of energy to generate direct current (DC) electricity. The power produced is fed into an inverter which will change the DC power to AC power in line with the requirements of the local utility grid provider. There are few key components used in the On-Grid solar solution such as photovoltaic (PV) panel arrays, Grid-Tie inverter, power meter and the electrical wirings.</p>', NULL, 'ongrid-solar-1578032794.jpg', NULL, NULL, NULL, 'ongrid-solar-inner-1578034100.jpg', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Solar', '<p>Business or homes can rely on On-Grid solar systems to meet their daily electricity requirements as well as to earn income from the excess power generated. On bright sunny days, buildings can generate enough solar energy to power appliances, lights, water heating systems etc&hellip;</p>', '<ul>\r\n<li>Residential users &amp; commercial owners can earn a passive income for the surplus energy generated by the system</li>\r\n<li>On-Grid solar systems are very cost effective and easy to install</li>\r\n<li>Business can recoup the cost of their investment by offsetting electricity bills in just 3-5 years</li>\r\n<li>No batteries for storage of electricity</li>\r\n<li>Low maintenance as solar panels need cleaning only and inverters last for 10 years</li>\r\n<li>Save money with Net-Metering</li>\r\n</ul>', NULL, NULL, NULL, NULL, NULL, 1578032795, 1578034100, 1, 1, NULL, 1, NULL, 'solution', 'on-grid-or-grid-tied'),
(11, 'WORK', 'Excellence through people', '<p>We understand that without people, organization cannot achieve their goals and without organization, people cannot fulfill their needs and desire. We look forward to open minded and hardworking people joining our team for a common goal which brings in growth to all the stakeholders. We promise career &amp; financial growth within the organization for the best people who deserve it.</p>', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WITH US', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1578036136, 1578036136, 1, 1, NULL, 1, NULL, 'basic', 'soleco-careers'),
(12, 'Sales Consultant – Solar Plants', '2 to 5 years’ experience in sales. Prior experience in renewable energy solutions sales.', '<ul>\r\n<li>Finding and setting up meetings with potential solar power plant customers, maintaining contact with existing customers, generating solar sales leads.</li>\r\n<li>Preparing proposals, contracts, and quotes</li>\r\n<li>Provide information to customers about solar power equipment and plants</li>\r\n<li>Provide information about costs savings, ROI calculations and incentives related to solar power plants</li>\r\n<li>Provide technical information on solar equipment products, prepare &amp; review detailed drawings for solar installations.</li>\r\n<li>Coordinate with local government authorities for approvals &amp; certifications</li>\r\n<li>Weekly sales pipeline update to the management./li&gt;</li>\r\n<li>Achieve the assigned sales target.</li>\r\n</ul>', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Sales Consultant', 'Engineering Degree in Electrical / Electronics', 'Kochi (positions are available across Kerala)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1578038277, 1578038277, 1, 1, 1, 1, NULL, 'career', 'sales-consultant---solar-plants'),
(13, 'Our', 'Extraordinary service for extraordinary customers.', '<p>We bring in rich and long experience, deep subject matter expertise, powerful assessment, capacity planning, diagnostics and automation tools and a sharp focus on results to deliver a superlative customer experience. We asses all aspects like people, process, technology and cost effectiveness before designing and implementing a solution. We address your implementation, business continuity and investment risks through our broad portfolio of service offerings.</p>', '<p><strong>Dedicated to serve our customers.</strong></p>', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Services', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1578152495, 1578152495, 1, 1, NULL, 1, NULL, 'advanced', 'soleco-services'),
(14, 'Individual Homes', NULL, '<p>Solar panels are a great way to offset energy costs, reduce the environment impact of your home and contributing to energy independence. Although sunny days will produce more solar energy, solar panels will continue to draw energy even the weather is cloudy. Solar panels also extend the life of a roof by protecting them from rain, snow, and heat. They make the house more energy efficient in the summer because the hot sun is not beating down on the roof directly &ndash; it is instead being absorbed by the panels, keeping the house temperature lower. <br /><br />We SOLECO, will help you design and implement a solution that best required for your house, keeping in mind the savings are 80% of your electricity bills.</p>', '<h3>Benefit of going Solar for homes</h3>\r\n<ul class=\"list-styled\">\r\n<li>No more power failures</li>\r\n<li>Very minimal dependency on utility grid</li>\r\n<li>Power independence especially in natural calamities and flood like situations</li>\r\n<li>Revenue generation utilizing the unused roof space</li>\r\n<li>Take advantage of government schemes and subsidies</li>\r\n<li>Faster return on investment.</li>\r\n</ul>', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'On Grid Solar,Off Grid Solar,Hybrid Solar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1578484950, 1578484950, 1, 1, 1, 0, NULL, 'solarsolution', 'individual-homes'),
(15, 'Project Developers', NULL, '<p>Project / community developers today face huge challenges when it comes to the approval of the project due to unavailability of the required power from the utility grid provider. The power requirements for the construction phase and the roll out phase is very critical as it has direct financial impact on the project. We can help design a system to help overcome these limitations and get the project out of the ground.<br /><br />We create microgrid and smart grid solutions which will be completely off grid where all the individual properties within the community can be interlinked to help each other to meet the peak time power requirements. The property management company can use power from the locally generated solar plants to meet the power requirements of common areas, lifts, swimming pools and garden areas.</p>', '<h3>Benefits of going solar for projects</h3>\r\n<ul class=\"list-styled\">\r\n<li>Avoid the delays in project approval</li>\r\n<li>Reduce the electricity cost during the construction phase</li>\r\n<li>Value addition to the property sales</li>\r\n<li>Future cost saving for the property management company &amp; tenants</li>\r\n<li>Reliable, local energy supply &ndash; Energy Security</li>\r\n<li>Community control and ownership</li>\r\n</ul>', NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1578485400, 1578485400, 1, 1, NULL, 0, NULL, 'solarsolution', 'project-developers'),
(16, 'Enterprises', NULL, '<p>The threat of continued electricity price rise poses significant challenges for any business in the region. By installing a commercial solar power system can reduce electricity costs immediately and significantly cut business risk going forward. Installing a commercial solar system in a business works in conjunction with your existing grid electricity supply. The electricity produced by your solar power system offsets the amount of electricity you need to purchase from your utility provider, thus reducing your electricity bill. <br /><br />The amount of your electricity that a solar power system can offset will depend on the size of the solar system you install and the amount of electricity you consume in daylight hours. SOLECO can provide you with detailed analysis on the most appropriate size of system based on: <strong>Your available roof space / open space for mounting solar panels, </strong> <strong>Your electricity consumption patterns (Normal, peak &amp; off peak), </strong> &nbsp;&nbsp;<strong>Power feed feasibility from your utility provider, </strong> <strong>Your budget</strong></p>', '<h3>Benefit of going Solar for Enterprises</h3>\r\n<p>The business case for installing a solar system on your commercial <span id=\"dots\">...</span><span id=\"more\">premises is now very compelling. The price of solar power has declined dramatically over the past years and this, coupled with recent electricity price rises has meant that the business case for solar is now extremely attractive</span></p>\r\n<p><button id=\"myBtn\" class=\"btn-primary btn-more\">Read more</button></p>\r\n<ul class=\"list-styled\">\r\n<li>High Investment Returns</li>\r\n<li>Immediate Cost Reduction</li>\r\n<li>Reduce Business Risk</li>\r\n<li>Reduce Carbon Emissions</li>\r\n<li>Shading of your roof and associated cooling effects</li>\r\n<li>Green incentives from the government</li>\r\n</ul>', NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1578486357, 1578486357, 1, 1, NULL, 0, NULL, 'solarsolution', 'enterprises'),
(17, 'Agriculture', NULL, '<p>Agriculture and farming are key to the survival and existence of life on the planet. Energy needs in farming &amp; agriculture sector have substantially increased over a period of time as farmers shifted from traditional farming practices to modern technology driven methods. For most farmers, electricity cost is the biggest expenditure and solar&nbsp;energy can be adapted in agriculture&nbsp;in a number of ways, saving money, increasing self-reliance, and reducing pollution.&nbsp;A growing number of farms and agricultural businesses are looking to solar to power their daily operations and the cost of going solar has declined in last few years, enabling more installations across the country. <br /><br />Solar based water pumping systems may be the most cost-effective water pumping option in locations where there is no existing power line. These power systems run pumps directly when the sun is shining, so they work hardest in the hot summer months when they are needed most. Generally, batteries are not necessary because the water is stored in tanks or pumped to fields and used in the day time. When properly sized and installed, PV water pumps are very reliable and require little maintenance. The size and cost of a PV water pumping system depends on the quality of solar energy available at the site, the pumping depth, the water demand, and system purchase and installation costs. <br /><br />Agricultural technology is changing rapidly. Farm machinery, farm buildings and production facilities are constantly being improved. Cheaper and improved sources of energy are needed for efficient and smooth operations of the facilities. These sources of energy are clean, risk-free and constitute no harm to man and environment.</p>', '<h3>Benefits of going solar in Agriculture</h3>\r\n<ul class=\"list-styled\">\r\n<li>Decrease energy costs and increase productivity</li>\r\n<li>Lock the future energy cost</li>\r\n<li>Increase property value from solar power</li>\r\n<li>Support eco-friendly businesses and Green farming</li>\r\n</ul>', NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1578486568, 1578486568, 1, 1, NULL, 0, NULL, 'solarsolution', 'agriculture'),
(18, 'Residential Solar', 'No power failure:  No more electricity Bills', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1578486958, 1578486958, 1, 1, NULL, 1, NULL, 'basic', 'residential-solar'),
(19, 'Commercial Solar', 'Today\'s investment is tomorrow\'s savings', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1578487001, 1578487001, 1, 1, NULL, 1, NULL, 'basic', 'commercial-solar');

-- --------------------------------------------------------

--
-- Table structure for table `node_image`
--

DROP TABLE IF EXISTS `node_image`;
CREATE TABLE IF NOT EXISTS `node_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nid` int(11) DEFAULT NULL,
  `node_image` varchar(255) DEFAULT NULL,
  `node_image_title` varchar(255) DEFAULT NULL,
  `node_image_description` text,
  `date` int(11) DEFAULT NULL,
  `text1` varchar(255) DEFAULT NULL,
  `text2` varchar(255) DEFAULT NULL,
  `default` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `article_id` (`nid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `node_image`
--

INSERT INTO `node_image` (`id`, `nid`, `node_image`, `node_image_title`, `node_image_description`, `date`, `text1`, `text2`, `default`) VALUES
(1, 14, 'Residential-Solar-1-1578485100.jpg', 'Residential Solar 1', 'Residential Solar 1', NULL, NULL, NULL, 0),
(2, 14, 'Residential-Solar-2-1578485100.jpg', 'Residential Solar 2', 'Residential Solar 2', NULL, NULL, NULL, 0),
(3, 15, 'Project-developer-1-1578485400.jpg', 'Project developer 1', NULL, NULL, NULL, NULL, 0),
(4, 15, 'Project-developer-2-1578485400.jpg', 'Project developer 2', NULL, NULL, NULL, NULL, 0),
(5, 16, 'Enterprises-1-1578486357.jpg', 'Enterprises 1', NULL, NULL, NULL, NULL, 0),
(6, 16, 'Enterprises-2-1578486357.jpg', 'Enterprises 2', NULL, NULL, NULL, NULL, 0),
(7, 17, 'Agriculture-1-1578486569.jpg', 'Agriculture 1', NULL, NULL, NULL, NULL, 0),
(8, 17, 'Agriculture-2-1578486569.jpg', 'Agriculture 2', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `node_sub`
--

DROP TABLE IF EXISTS `node_sub`;
CREATE TABLE IF NOT EXISTS `node_sub` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `summary` text,
  `description` text,
  `other` text,
  `list` text,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sub_node` (`nid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `node_sub`
--

INSERT INTO `node_sub` (`id`, `nid`, `title`, `sub_title`, `summary`, `description`, `other`, `list`, `image`) VALUES
(1, 7, 'Vision', '', 'To be recognized as the leading and most preferred regional provider of Renewable Energy solutions to individuals and organizations to be energy independent.', '<p><strong>&ldquo;Bring the power back in the hands of people&rdquo;</strong></p>', '', NULL, 'vision-1573128456.jpg'),
(2, 7, 'MIssion', '', 'By the end of year 2022, SolEco has set itself to become a leader in specified energy sector technologies, in selective markets all over India & abroad by striving towards the following', '<p><strong>&ldquo;Contribute to the Clean Nation drive&rdquo;</strong></p>', '<ul class=\"list-styled\">\r\n<li>Exceeding customer expectations in terms of quality of products and services</li>\r\n<li>Building and retaining a professional team of people</li>\r\n<li>Developing a portfolio of industry leading, technology portfolios</li>\r\n<li>Building close and effective partnerships</li>\r\n</ul>', NULL, 'mission-1573128438.jpg'),
(3, 13, 'PROJECT', 'METHODOLOGY', '', '<p>SOLECO follows simple and easy project management methodologies that will ensure a successful and quality implementation. We understand the customer needs and carefully tailor make the implementation process and monitor each step. Traditional project management is practiced and includes specific techniques that are applied to the planning, estimating and control of the activities that make up a project. Below is the process involved in a typical project and we strictly follow the same.</p>', '<h3>Pre- Sales Process</h3>\r\n<ul class=\"list-styled\">\r\n<li>Defining the objectives</li>\r\n<li>Understanding the current challenges</li>\r\n<li>Technical Feasibility</li>\r\n<li>Return on Investment</li>\r\n<li>Conceptual design</li>\r\n<li>Revalidation of the objectives</li>\r\n<li>Commercial Proposal</li>\r\n</ul>', '<h3>Post-sales Process</h3>\r\n<ul class=\"list-styled\">\r\n<li>Site Assessment</li>\r\n<li>Detailed work breakdown Structure</li>\r\n<li>Project plan acceptance</li>\r\n<li>Sourcing</li>\r\n<li>Implementation</li>\r\n<li>Testing &amp; Approval</li>\r\n<li>Commissioning &amp; Handover</li>\r\n</ul>', NULL),
(4, 13, 'operations &', 'maintenance', '', '<p>The need of operations &amp; maintenance (O&amp;M) in solar PV industry is becoming essential as there are many plants being built in residential &amp; commercial segments in the region. These plants require periodic maintenance &amp; services to ensure the smooth functioning and expected output. We SOLECO offers O&amp;M services to our valued customers which determines the overall performance &amp; profitability of the plant. Our industry experience will help you increase the plant efficiency by optimizing the plant&rsquo;s performance. Our post- delivery support model is very successful with most of our customers by meeting their support expectation and business continuity goals.</p>\r\n<p>We have various combination of support services offerings with strict Service Level Agreements that meets the customer requirements. Customers can leverage the technical expertise for</p>', '<ul class=\"list-styled\">\r\n<li>Periodic health check services</li>\r\n<li>Planned maintenance</li>\r\n<li>Corrective maintenance</li>\r\n<li>Post Implementation improvements</li>\r\n<li>Performance management</li>\r\n<li>Optimal spares management</li>\r\n<li>Critical Break down incidents management</li>\r\n</ul>', '', 'img-1-1578153918.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `module` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(510) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `title`, `module`, `name`, `value`) VALUES
(1, 'Image Upload Location', 'site', 'path', '../uploads'),
(2, 'Image Upload Url', 'site', 'url', 'http://localhost/soleco/uploads/'),
(3, 'Site name', 'base', 'name', 'Soleco'),
(4, 'CSS file location', 'site', 'css', '../webassets/css/'),
(5, 'JS file location', 'site', 'js', '../webassets/js/'),
(6, 'Template Path', 'site', 'template', '@frontend/views/template/'),
(7, 'Layout Path', 'site', 'layout', '@frontend/views/layouts/'),
(8, 'Widget Path', 'site', 'widget', '@frontend/views/widgets/'),
(9, 'Block path', 'site', 'block', '@frontend/views/blocks/'),
(10, 'SMTP Host', 'smtp', 'host', 'smtp.mailtrap.io'),
(11, 'SMTP Username', 'smtp', 'username', 'cac91bdf122d99'),
(12, 'SMTP Password', 'smtp', 'password', '28f769256857be'),
(13, 'SMTP Port', 'smtp', 'port', '2525'),
(14, 'SMTP Encryption', 'smtp', 'encryption', 'tls'),
(15, 'Facebook', 'custom', 'facebook', '#'),
(16, 'Twitter', 'custom', 'twitter', '#'),
(17, 'Instagram', 'custom', 'instagram', '#'),
(25, 'SMTP Email', 'smtp', 'email', 'vinod-560b68@inbox.mailtrap.io'),
(35, 'Email', 'custom', 'email', 'info@soleco.com'),
(36, 'Whatsapp', 'custom', 'whatsapp', '+91 9961460006'),
(37, 'Whatsapp Text', 'custom', 'whatsapp-text', 'Hi i would like to get details regarding Soleco Products'),
(38, 'Get in Touch', 'custom', 'get-in-touch', '+91 484 2306006'),
(39, 'Office Address', 'custom', 'office-address', 'SOLECO ENERGY PRIVATE LIMITED\r\n51/537 (C), First Floor, Highway Arcade, Victor Leenus Lane, Thykoodam, Vyttila. P.O, Kochi, 682019.'),
(40, 'Office Phone', 'custom', 'office-phone', '+91 4842306006'),
(41, 'Office Email', 'custom', 'office-email', 'sales@soleco.in'),
(44, 'Welcome', 'img', 'welcome', '17'),
(45, 'Why Soleco', 'img', 'why-soleco', '18'),
(46, 'Head Office Address', 'custom', 'head-office-address', 'INTEGRALTECH NETWORKS LLC\r\nOffice # 208, Hadi Plaza, Dubai, UAE.'),
(47, 'Logo', 'img', 'logo', '16'),
(48, 'Head Office Phone', 'custom', 'head-office-phone', '+971 4 2567650'),
(49, 'Head Office Web', 'custom', 'head-office-web', 'www.integralnetworks.ae'),
(50, 'Career Mail', 'custom', 'career-mail', 'admin@soleco.in');

-- --------------------------------------------------------

--
-- Table structure for table `source_message`
--

DROP TABLE IF EXISTS `source_message`;
CREATE TABLE IF NOT EXISTS `source_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `idx_source_message_category` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `source_message`
--

INSERT INTO `source_message` (`id`, `category`, `message`) VALUES
(1, '*', 'copyright'),
(2, '*', 'footer_note'),
(3, '*', 'email_error'),
(4, '*', 'email_success'),
(5, '*', 'career_message'),
(6, '*', 'career_list_message1'),
(7, '*', 'career_list_message2');

-- --------------------------------------------------------

--
-- Table structure for table `template`
--

DROP TABLE IF EXISTS `template`;
CREATE TABLE IF NOT EXISTS `template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `style` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `file` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `template`
--

INSERT INTO `template` (`id`, `style`, `type`, `file`) VALUES
(1, 'list', 'basic', 'listing-page.php'),
(2, 'list', 'service', 'listing-page.php'),
(3, 'list', 'package', 'package-listing.php'),
(4, 'detail', 'package', 'details-page.php'),
(5, 'detail', 'package', 'package-details.php'),
(6, 'detail', 'category', 'details-page.php'),
(7, 'detail', 'service', 'details-page.php'),
(8, 'detail', 'advanced', 'details-page.php'),
(9, 'detail', 'basic', 'details-page.php'),
(10, 'list', 'advanced', 'listing-page.php');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'admin', 'W_4ZWWcpQZQ-wc5LuLwQ6UPasB8VZs_U', '$2y$13$pL7EZ3LR7ghLcC/Vb8UDAufOOlwefgRqPDRGKceKs/05SIdpxVwTG', NULL, 'admin@admin.com', 10, 1564485595, 1564485595, '0C7B0Hk63uCB-27o6T9_sP9PgZfvq75G_1564485595');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `block_image`
--
ALTER TABLE `block_image`
  ADD CONSTRAINT `fk_block_images` FOREIGN KEY (`b_id`) REFERENCES `block` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `fk_message_source_message` FOREIGN KEY (`id`) REFERENCES `source_message` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `node_image`
--
ALTER TABLE `node_image`
  ADD CONSTRAINT `fk_node_image` FOREIGN KEY (`nid`) REFERENCES `node` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `node_sub`
--
ALTER TABLE `node_sub`
  ADD CONSTRAINT `fk_sub_node` FOREIGN KEY (`nid`) REFERENCES `node` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
