<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\helpers;

/**
 * Url provides a set of static methods for managing URLs.
 *
 * For more details and usage information on Url, see the [guide article on url helpers](guide:helper-url).
 *
 * @author Alexander Makarov <sam@rmcreative.ru>
 * @since 2.0
 */
class Url extends \yii\helpers\BaseUrl
{
    public static function home($absolute = false)
    {
		return static::to(['site/index'],$absolute);
    }
    public static function node($node,$absolute = false)
    {

        $id = $model = '';
        if (strpos($node, '/node/') !== false) {
        $id = str_replace('/node/','',$node);
        $model = \backend\models\Node::findOne($id);
        }
        if ($model) {
           if(!empty($model->slug)){
               if($model->base == 1){
                    return static::to(['site/slug','slug' => $model->slug],$absolute);
               }else{
                    return static::to(['site/detail','type' => $model->type,'slug' => $model->slug],$absolute);
               }
            }else{
                return static::to(['site/view','id' => $model->id],$absolute);
            }

        }
        elseif(Node::find()->where(['type' => \yii\helpers\Inflector::singularize($node)])->one()){
        return static::to(['site/slug','slug' => $node],$absolute);
        }
        else{
         return static::to(['node/view','id' => $id],$absolute); 
        }

    }
}
