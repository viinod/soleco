<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\helpers;

/**
 * Url provides a set of static methods for managing URLs.
 *
 * For more details and usage information on Url, see the [guide article on url helpers](guide:helper-url).
 *
 * @author Alexander Makarov <sam@rmcreative.ru>
 * @since 2.0
 */
class Cms extends \yii\helpers\BaseStringHelper
{
    public static function clean($string) {
       $string = str_replace(' ', '-', $string); 
       return preg_replace('/[^A-Za-z0-9\-]/', '', $string); 
    }
}
