<?php

namespace common\components;
use Yii;
use yii\base\Behavior;

class Image extends Behavior
{         
        public function events()
        {
                return [
                \yii\web\application::EVENT_BEFORE_REQUEST => 'imageSettings',
                ];
        }
        public function imageSettings()
        {
                Yii::setAlias('@path', Yii::$app->params['site.path']);
                Yii::setAlias('@url', Yii::$app->params['site.url']);
        }

}
?>
