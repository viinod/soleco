<?php

namespace common\components;

use Yii;
use yii\base\Component;
class Settings extends Component
{         
        public function custom($name)
        {
           
            return isset(Yii::$app->params['custom.'.$name]) ? Yii::$app->params['custom.'.$name] : NULL;
        }
        public function base($name)
        {
           
            return isset(Yii::$app->params['base.'.$name]) ? Yii::$app->params['base.'.$name] : NULL;
        }
        public function img($name)
        {
           
            return isset(Yii::$app->params['img.'.$name]) ? Yii::$app->params['img.'.$name] : NULL;
        }
}
?>
