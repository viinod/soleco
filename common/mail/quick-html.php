<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <meta name="viewport" content="minimum-scale=1.0, maximum-scale=1.0, width=device-width, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <title>Hero Paints</title>
    <style type="text/css">
	body{ margin:0; padding:50px 40px;}
	table {border-collapse: collapse;}
	td {border: 1px solid #898888;}
	th { height: 50px;}
	th {text-align: left;}
	td { height: 40px; vertical-align: center; padding:0 0 0 20px;}
	.hea{ font-family: Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold; text-align:center; background-color:#f2efef; height:40px; color:#fff;}
	.hea2{ text-align:left; font-family: Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold; color:#fff; background-color:#05b9f2;}
	.hea3{ background-color:#f2efef;}
	.hea4{ font-family:Arial, Helvetica, sans-serif; font-size:12px; }
	.items{ font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;}
	.items2{ height:100px}
	@media screen and (max-width: 480px) { 
	body{ margin:0; padding:10px;}
	td{ padding:0 0 0 3px;}
	}
</style>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="hea2">Quick Contact from <?= $data['name'] ?></td>
      </tr>
      <tr>
        <td width="19%" class="items hea3">Name:-</td>
        <td width="81%" align="left" valign="center" class="hea4"><?= $data['name'] ?></td>
      </tr>
      <tr>
        <td width="19%" class="items hea3">Email:-</td>
        <td width="81%" align="left" valign="center" class="hea4"><?= $data['email'] ?></td>
      </tr>
      <tr>
        <td width="19%" class="items hea3">Subject:-</td>
        <td width="81%" align="left" valign="center" class="hea4"><?= $data['subject'] ?></td>
      </tr>   
      <tr>
        <td width="19%" class="items hea3">Message:-</td>
        <td width="81%" align="left" valign="center" class="hea4  items2">
          <?= $data['message'] ?>
        </td>
      </tr>   
    </table>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
