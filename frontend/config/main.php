<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'imagemanager' => [
        'class' => 'noam148\imagemanager\components\ImageManagerGetPath',
        //set media path (outside the web folder is possible)
        'mediaPath' => 'common/media',
        //path relative web folder to store the cache images
        'cachePath' =>  ['assets/images', 'assets/images'],
        //use filename (seo friendly) for resized images else use a hash
        'useFilename' => true,
        //show full url (for example in case of a API)
        'absoluteUrl' => false,
    ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            '/' => 'site/index',
            'site/contact' => 'site/contact',
            'site/captcha' => 'site/captcha',             
            [
                'pattern' => '<alias:download>/<slug:[A-Za-z0-9 -_.]+>',
                'route' => 'site/<alias>',
                'suffix' => '.pdf',
            ],
            'node/<id:\d+>' => 'site/view',
            '<type:[A-Za-z0-9-]+>/<slug:[A-Za-z0-9 -_.]+>' => 'site/detail',
            '<slug:[A-Za-z0-9 -_.]+>' => 'site/slug',

            ],
        ],
        'assetManager' => [
            'linkAssets' => true,
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,
                    'baseUrl' => 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/',
                    'js' => [
                        'jquery.min.js'
                    ],
                ],
                'yii\bootstrap4\BootstrapAsset' => [
                    'sourcePath' => null,
                    'baseUrl' => 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/',
                    'css' => [
                        'bootstrap.min.css'
                    ],
                ],
                'yii\bootstrap4\BootstrapPluginAsset' => [
                    'sourcePath' => null,
                    'baseUrl' => 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/',
                    'js' => [
                        'bootstrap.bundle.min.js'
                    ],
                ],
                
            ],
        ],
        
    ],
    'params' => $params,
];
