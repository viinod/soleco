<?php
use yii\helpers\Html;
use common\helpers\Url;
use yii\bootstrap\ActiveForm;
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
$solutions =  \backend\models\Solution::find()->where(['type' => 'solution'])->all();
$this->title = 'Soleco | Our Solutions';
?>
<section class="inner-bg1 cd-section">
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
<div class="carousel-inner">
<div class="carousel-item active">
<img class="d-block w-100" src="<?=$directoryAsset?>/images/solutions.jpg" alt="First slide">
<div class="carousel-caption">
<div class="row justify-content-md-center">
<div class="col-lg-8 col-sm-12 carausel-text inner-text">
<div class="page-title">
<h2>Solutions</h2>
<p><?=$model->summary?></p>
</div>
<div class="breadcrumb-area">
<ul class="list-inline">
<li class="list-inline-item"><a href="<?=Url::home()?>"><i class="fas fa-home"></i></a></li>
<li class="list-inline-item seperation">/</li>
<li class="list-inline-item">Solutions</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="navbar-area" id="fixedmenu">
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="cd-vertical-nav">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
<div class="navbar-nav">
<a class="nav-item nav-link" href="#section1">Solutions<span class="sr-only">(current)</span></a>
<a class="nav-item nav-link" href="#section2">Our Portfolio</a>
</div>
</div>
</nav>
</div>
<div id="section1"></div>
</section>
<section class="solution-section" data-scrollreveal="enter top over 2s after 0.5s">
<div class="custome-fluid">
<div class="row">
<div class="col-sm-12 center-head">
<h2><?=$model->title?> <span><?=$model->sub_title?></span></h2>
<?=$model->description?>
</div>
<div class="col-sm-12 owl-1 owl-theme owl-slides" data-scrollreveal="enter bottom over 2s after 0.5s">
<?php foreach ($solutions as $solution): ?>
<div class="item">
<div class="row">
<div class="col-sm-12 profile">
<div class="solution-details">
<h4><?=$solution->title?><span><?=$solution->sub_title?></span></h4>
<p><?=$solution->summary?></p>
<a class="btn-primary btn-more btn-more-2" href="#<?=$solution->slug?>" data-lity>Continue Reading</a>
</div>
<img src="<?=Yii::getAlias('@url').'/solutions/'.$solution->image?>" alt="solution">
</div>
</div>
</div>
<?php endforeach ?>
</div>
</div>
</div>
<div id="section2"></div>
</section>
<section class="solution-area">
<div class="container-fluid custome-fluid">
<div class="row">
<div class="col-sm-12 center-head" data-scrollreveal="enter top over 2s after 0.5s">
<?=\frontend\widgets\Block::widget(['id' => 4])?>
</div>
<div class="col-sm-12 solution-landing">
<div class="row justify-content-md-center">
<div class="col-sm-5 hvr-float link-solutions" data-scrollreveal="enter top over 1s after 0.5s">
<a href="solutions-residential.html">
<div class="residential">
<img src="<?=$directoryAsset ?>/images/icon-2.png" alt="home">
<?=\frontend\widgets\Block::widget(['id' => 5])?>
</div>
</a>
</div>
<div class="col-sm-5 hvr-float link-solutions" data-scrollreveal="enter top over 1s after 0.5s">
<a href="solutions-commercial.html">
<div class="commercial">
<img src="<?=$directoryAsset ?>/images/icon-1.png" alt="home">
<?=\frontend\widgets\Block::widget(['id' => 6])?>
</div>
</a>
</div>
</div>
</div>
</div>
</div>
</section>
<?php foreach ($solutions as $sol): ?>
<div id="<?=$sol->slug?>" style="background:#fff" class="lity-hide lity-styles readmore-lity">
<div class="container-fluid content-area contact-section">
<div class="row">
<div class="col-sm-12 read-more-area solution-area">
<div class="popup-content-img">
<img src="<?=Yii::getAlias('@url').'/solutions/'.$sol->file?>" alt="solution">
</div>
<h4><?=$sol->title?><span><?=$sol->sub_title?></span></h4>
<?=$sol->description?>
<h4>Applications</h4>
<?=$sol->text1?>
<h4>Advantages</h4>
<?=$sol->text2?>
</div>
</div>
</div>
</div>
<?php endforeach ?>