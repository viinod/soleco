<?php
$this->title = 'Hero Paints | '.$model->title;
use common\helpers\Url;
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
$categories = \backend\models\Category::find()->all();
$products = \backend\models\Product::find()->where(['type' => 'product'])->all();
?>
<section>
<div class="inner-bg">
</div>
<div class="slider-overlay">
<div class="breadcrum-area">
<div class="container">
<div class="row">
<div class="col-sm-6 page-title">
<h2><?=$model->title?></h2>
</div>
<div class="col-sm-6 site-breadcrumb">
<ul class="list-inline">
<li class="list-inline-item home-link"><a href="<?=Url::home()?>">Home</a></li>
<li class="list-inline-item sepration">/</li>
<li class="list-inline-item"><?=$model->title?></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</section>
<section>
<div class="container">
<div class="row">
<div class="col-sm-12 brand-area-headding">
<h2><?=$model->title?></h2>
<?=$model->description?>
</div>
<main class="col-sm-12 cd-main-content">
<div class="cd-tab-filter-wrapper">
<div class="cd-tab-filter">
<ul class="cd-filters">
<li class="placeholder">
<a data-type="all" href="#0">All</a>
</li>
<li class="filter"><a class="selected" href="#0" data-type="all">All</a></li>
<?php foreach ($categories as $category) :?>
<li class="filter" data-filter=".<?=$category->title?>"><a href="#0" data-type="<?=$category->title?>"><?=$category->title?></a></li>
<?php endforeach;?>
</ul>
</div>
</div>
<section class="cd-gallery">
<ul>
<?php foreach ($products as $product) :?>
<li class="mix check1 radio2 option3 <?=$product->other?>">
<div class="col-sm-12 product-details">
<figure><a href="#"><img src="<?=Yii::getAlias('@url').'/products/'.$product->image?>" alt="awards"></a></figure>
<div class="product-title">
<h4><?=$product->title?></h4>
<a href="<?=Url::node('/node/'.$product->id)?>">Read More <i class="fas fa-arrow-circle-right"></i></a>
</div>
</div>
</li>
<?php endforeach;?>
<li class="gap"></li>
<li class="gap"></li>
<li class="gap"></li>
</ul>
<div class="cd-fail-message">No results found</div>
</section>
<a href="#0" class="cd-filter-trigger"></a>
</main>
</div>
</div>
</section>


