<?php
use yii\helpers\Html;
use common\helpers\Url;
use yii\bootstrap\ActiveForm;
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
$solutions =  \backend\models\Solution::find()->where(['type' => 'solution'])->all();
$this->title = 'Soleco | Our Solutions';
?>
<section class="inner-bg1 cd-section">
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
<div class="carousel-inner">
<div class="carousel-item active">
<img class="d-block w-100" src="<?=$directoryAsset?>/images/solutions.jpg" alt="First slide">
<div class="carousel-caption">
<div class="row justify-content-md-center">
<div class="col-lg-8 col-sm-12 carausel-text inner-text">
<div class="page-title">
<h2>Services</h2>
<p><?=$model->summary?></p>
</div>
<div class="breadcrumb-area">
<ul class="list-inline">
<li class="list-inline-item"><a href="<?=Url::home()?>"><i class="fas fa-home"></i></a></li>
<li class="list-inline-item seperation">/</li>
<li class="list-inline-item">Services</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="navbar-area" id="fixedmenu">
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="cd-vertical-nav">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
<div class="navbar-nav">
<a class="nav-item nav-link" href="#section1">Our Services <span class="sr-only">(current)</span></a>
<a class="nav-item nav-link" href="#section2">Project Methodology</a>
<a class="nav-item nav-link" href="#section3">Operations &amp; Maintenance</a>
<a class="nav-item nav-link" href="#section4">Our Portfolio</a>
</div>
</div>
</nav>
</div>
<div id="section1"></div>
</section>
<section class="project-section" data-scrollreveal="enter top over 2s after 0.5s">
<div class="container-fluid">
<div class="row justify-content-md-center">
<div class="col-lg-8 col-sm-12 center-head">
<h2><?=$model->title?><span><?=$model->sub_title?></span></h2>
<?=$model->description?>
<?=$model->other?>
</div>
</div>
</div>
<div id="section2"></div>
</section>
<section class="service-level-area" data-scrollreveal="enter top over 2s after 0.5s">
<?php if(isset($model->nodeSubs[0])): ?>
<div class="container-fluid custome-fluid">
<div class="row justify-content-md-center">
<div class="col-sm-12 center-head">
<h2><?=$model->nodeSubs[0]->title?> <span><?=$model->nodeSubs[0]->sub_title?> </span></h2>
<?=$model->nodeSubs[0]->description?></div>
<div class="col-lg-6 col-sm-12 operation-text solution-inner-text">
<div class="operation-details">
<div class="row justify-content-md-center">
<div class="col-lg-10 col-sm-12 left-head">
<?=$model->nodeSubs[0]->other?>
</div>
</div>
</div>
</div>
<div class="col-lg-6 col-sm-12 operation-text solution-inner-text">
<div class="operation-details">
<div class="row justify-content-md-center">
<div class="col-lg-10 col-sm-12 left-head">
<?=$model->nodeSubs[0]->list?>
</div>
</div>
</div>
</div>
</div>
</div>
<?php endif; ?>
<div id="section3"></div>
</section>
<section class="operation-area" data-scrollreveal="enter top over 2s after 0.5s">
<?php if(isset($model->nodeSubs[1])): ?>
<div class="container-fluid custome-fluid">
<div class="row">
<div class="col-lg-5 col-sm-12 operation-bg no-padding">
<img src="<?=Yii::getAlias('@url').'/advanced/'.$model->nodeSubs[1]->image?>" alt="operation">
</div>
<div class="col-lg-7 col-sm-12 operation-text">
<div class="operation-details">
<div class="row justify-content-md-center">
<div class="col-lg-9 col-sm-12 left-head">
<h2><?=$model->nodeSubs[1]->title?> <span><?=$model->nodeSubs[1]->sub_title?> </span></h2>
<?=$model->nodeSubs[1]->description?> 
<?=$model->nodeSubs[1]->other?> 
</div>
</div>
</div>
</div>
</div>
</div>
<?php endif; ?>
<div id="section4"></div>
</section>
<section class="solution-area">
<div class="container-fluid custome-fluid">
<div class="row">
<div class="col-sm-12 center-head" data-scrollreveal="enter top over 2s after 0.5s">
<?=\frontend\widgets\Block::widget(['id' => 4])?>
</div>
<div class="col-sm-12 solution-landing">
<div class="row justify-content-md-center">
<div class="col-sm-5 hvr-float link-solutions" data-scrollreveal="enter top over 1s after 0.5s">
<a href="solutions-residential.html">
<div class="residential">
<img src="<?=$directoryAsset ?>/images/icon-2.png" alt="home">
<?=\frontend\widgets\Block::widget(['id' => 5])?>
</div>
</a>
</div>
<div class="col-sm-5 hvr-float link-solutions" data-scrollreveal="enter top over 1s after 0.5s">
<a href="solutions-commercial.html">
<div class="commercial">
<img src="<?=$directoryAsset ?>/images/icon-1.png" alt="home">
<?=\frontend\widgets\Block::widget(['id' => 6])?>
</div>
</a>
</div>
</div>
</div>
</div>
</div>
</section>
<?php foreach ($solutions as $sol): ?>
<div id="<?=$sol->slug?>" style="background:#fff" class="lity-hide lity-styles readmore-lity">
<div class="container-fluid content-area contact-section">
<div class="row">
<div class="col-sm-12 read-more-area solution-area">
<div class="popup-content-img">
<img src="<?=Yii::getAlias('@url').'/solutions/'.$sol->file?>" alt="solution">
</div>
<h4><?=$sol->title?><span><?=$sol->sub_title?></span></h4>
<?=$sol->description?>
<h4>Applications</h4>
<?=$sol->text1?>
<h4>Advantages</h4>
<?=$sol->text2?>
</div>
</div>
</div>
</div>
<?php endforeach ?>