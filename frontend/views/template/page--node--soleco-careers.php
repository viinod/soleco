<?php
use yii\helpers\Html;
use common\helpers\Url;
use yii\bootstrap\ActiveForm;
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
$careers =  \backend\models\Career::find()->where(['type' => 'career','status' => 1])->all();
$i = 1;
$this->title = 'Soleco | Careers';
?>
<section class="inner-bg1 cd-section">
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
<div class="carousel-inner">
<div class="carousel-item active">
<img class="d-block w-100" src="<?=$directoryAsset?>/images/solutions.jpg" alt="First slide">
<div class="carousel-caption">
<div class="row justify-content-md-center">
<div class="col-lg-8 col-sm-12 carausel-text inner-text">
<div class="page-title">
<h2>Careers</h2>
<p><?=$model->summary?></p>
</div>
<div class="breadcrumb-area">
<ul class="list-inline">
<li class="list-inline-item"><a href="<?=Url::home()?>"><i class="fas fa-home"></i></a></li>
<li class="list-inline-item seperation">/</li>
<li class="list-inline-item">Careers</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="navbar-area" id="fixedmenu">
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="cd-vertical-nav">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
<div class="navbar-nav">
<a class="nav-item nav-link" href="#section1">Work With Us<span class="sr-only">(current)</span></a>
<a class="nav-item nav-link" href="#section2">Our Portfolio</a>
</div>
</div>
</nav>
</div>
<div id="section1"></div>
</section>
<section class="mission-area job-area">
<div class="container-fluid custome-fluid">
<div class="row">
<div class="col-sm-12 center-head" data-scrollreveal="enter left over 2s after 0.5s">
<h2><?=$model->title?> <span><?=$model->sub_title?></span></h2>
<?=$model->description?>
</div>
<div class="col-sm-12 career-listing">
<div class="row">
<?php foreach ($careers as $career): ?>
<div class="col-lg-6 col-sm-12 hvr-float service-items" data-scrollreveal="enter top over 2s after 0.5s">
<div class="services">
<div class="row">
<div class="col-lg-2 col-sm-12 career-number">
<h3><?=sprintf("%02d", $i)?>.</h3>
</div>
<div class="col-lg-10 col-sm-12 career-details no-padding">
<h2><?=$career->title?></h2>
<ul class="list-inline">
<li class="list-inline-item"><span>Post : </span><?=$career->field1?></li>
<li class="list-inline-item"><span>Location : </span><?=$career->field3?></li>
</ul>
<a href="<?=Url::node('/node/'.$career->id)?>"><i class="far fa-arrow-alt-circle-right"></i> View Details</a>
</div>
</div>
</div>
</div>
<?php $i++; endforeach ?>
</div>
</div>
</div>
</div>
</section>
<section class="mail-area">
<div class="container-fluid custome-fluid">
<div class="row">
<div class="col-sm-12 mail-to-us">
<h2><?=Yii::t('*', 'career_list_message1')?></h2>
<p><?=Yii::t('*', 'career_list_message2')?></p>
<a href="mailto:<?=Yii::$app->settings->custom('career-mail')?>" target="_top"><i class="far fa-arrow-alt-circle-right"></i>Post Now</a>
</div>
</div>
</div>
<div id="section2"></div>
</section>
<section class="solution-area">
<div class="container-fluid custome-fluid">
<div class="row">
<div class="col-sm-12 center-head" data-scrollreveal="enter top over 2s after 0.5s">
<?=\frontend\widgets\Block::widget(['id' => 4])?>
</div>
<div class="col-sm-12 solution-landing">
<div class="row justify-content-md-center">
<div class="col-sm-5 hvr-float link-solutions" data-scrollreveal="enter top over 1s after 0.5s">
<a href="solutions-residential.html">
<div class="residential">
<img src="<?=$directoryAsset ?>/images/icon-2.png" alt="home">
<?=\frontend\widgets\Block::widget(['id' => 5])?>
</div>
</a>
</div>
<div class="col-sm-5 hvr-float link-solutions" data-scrollreveal="enter top over 1s after 0.5s">
<a href="solutions-commercial.html">
<div class="commercial">
<img src="<?=$directoryAsset ?>/images/icon-1.png" alt="home">
<?=\frontend\widgets\Block::widget(['id' => 6])?>
</div>
</a>
</div>
</div>
</div>
</div>
</div>
</section>
