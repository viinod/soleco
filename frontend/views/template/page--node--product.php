<?php
use yii\helpers\Html;
use common\helpers\Url;
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
$first = true;
$this->title = 'Hero Paints | '.$model->title;
?>
<section>
<div class="inner-bg">
</div>
<div class="slider-overlay">
<div class="breadcrum-area">
<div class="container">
<div class="row">
<div class="col-sm-6 page-title">
<h2><?=$model->title?></h2>
</div>
<div class="col-sm-6 site-breadcrumb">
<ul class="list-inline">
<li class="list-inline-item home-link"><a href="<?=Url::home()?>">Home</a></li>
<li class="list-inline-item sepration">/</li>
<li class="list-inline-item home-link"><a href="<?=Url::node('/node/6')?>">Our Products</a></li>
<li class="list-inline-item sepration">/</li>
<li class="list-inline-item"><?=$model->title?></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</section>
<section>
<div class="container">
<div class="row justify-content-md-center">
<div class="col-sm-8 product-details">
<h2><?=$model->title?></h2>
<img src="<?=Yii::getAlias('@url').'/products/'.$model->image?>" alt="<?=$model->title?>">
<?=$model->description?>
<div class="row">
<div class="col-sm-4 enquire-now-area">
<button href="#applynow" class="btn btn-secondary" data-lity>Enquire Now</button>
</div>
<div class="col-sm-8 share-area">
<div class="sharethis-inline-share-buttons"></div>
</div>
</div>
</div>
</div>
</div>
</section>
<div id="applynow" style="background:#fff" class="lity-hide enquiry col-sm-12">
<?= Html::beginForm(['site/enquire'],'post',['id' => 'eform']) ?>
<div class="row">
<div class="col-sm-12 contact-form-head">
<h2>leave us <span>a message</span></h2>
<hr>
<div class="alert alert-success enquire-success alert-dismissable collapse">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<?=Yii::t('*', 'email_success')?>
</div>
<div class="alert alert-danger enquire-danger alert-dismissable collapse">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<?=Yii::t('*', 'email_error')?>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-6 col-sm-12 contact-form">
<div class="form-group">
<input type="text" name="name" required class="form-control" placeholder="Name">
</div>
</div>
<div class="col-lg-6 col-sm-12 contact-form">
<div class="form-group">
<input type="email" name="email" required class="form-control" placeholder="Email">
</div>
</div>
</div>
<div class="row">
<div class="col-lg-6 col-sm-12 contact-form">
<div class="form-group select-arrow">
<select class="form-control" name='subject' id="exampleFormControlSelect1">
<option value="Product Enquiry">Product Enquiry</option>
<option value="General Enquiry">General Enquiry</option>
</select>
</div>
</div>
<div class="col-lg-6 col-sm-12 contact-form">
<div class="form-group">
<input type="text" name="phone" required class="form-control" pattern="\d+" placeholder="Phone"></div>
</div>
</div>
<div class="row">
<div class="col-lg-12 col-sm-12 contact-form">
<div class="form-group">
<textarea class="form-control text-area" rows="5" name="message" placeholder="Message"></textarea>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-7 indication-text">
We will contact you within one business day.
</div>
<div class="col-sm-5 submit-area">
<div class="form-group">
<div id='loadingenquire' style='display:none'>
<img src='<?=$directoryAsset?>/images/ajax-loader.gif'/>
</div>
<button type="submit" class="btn btn-primary btn-lg" name="enquire-button" id="enquire-button">Submit</button>
</div>
</div>
</div>
<?= Html::endForm() ?>
</div>

