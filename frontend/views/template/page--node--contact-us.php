<?php
use yii\helpers\Html;
use common\helpers\Url;
use yii\bootstrap\ActiveForm;
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
$contact = new \frontend\models\ContactForm();
$this->title = 'Soleco | Contact Us';
?>
<section class="inner-bg1 cd-section">
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
<div class="carousel-inner">
<div class="carousel-item active">
<img class="d-block w-100" src="<?=$directoryAsset?>/images/bg4.jpg" alt="First slide">
<div class="carousel-caption">
<div class="row justify-content-md-center">
<div class="col-lg-8 col-sm-12 carausel-text inner-text">
<div class="page-title">
<h2>Contact Us</h2>
</div>
<div class="breadcrumb-area">
<ul class="list-inline">
<li class="list-inline-item"><a href="<?=Url::home()?>"><i class="fas fa-home"></i></a></li>
<li class="list-inline-item seperation">/</li>
<li class="list-inline-item">Contact Us</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="navbar-area" id="fixedmenu">
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="cd-vertical-nav">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
<div class="navbar-nav">
<a class="nav-item nav-link" href="#section1">Contact us <span class="sr-only">(current)</span></a>
<a class="nav-item nav-link" href="#section2">Our Portfolio</a>
</div>
</div>
</nav>
</div>
<div id="section1"></div>
</section>
<section class="contact-area">
<div class="container-fluid">
<div class="row">
<div class="col-sm-12 col-lg-6 col-md-12 contact-form-area">
<div class="col-sm-12 center-head">
<h2>Feel free to <span>contact us</span></h2>
<p>Please fill out the information below and our Sales Consultants will be in touch soon.</p>
<hr>
<div class="alert alert-success contact-success alert-dismissable collapse">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<?=Yii::t('*', 'email_success')?>
</div>
<div class="alert alert-danger contact-danger alert-dismissable collapse">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<?=Yii::t('*', 'email_error')?>
</div>
</div>
<?php $form = ActiveForm::begin(['action' => ['site/contact'],'id' => 'contact-form']); ?> 
<div class="row">
<div class="col-sm-12 contact-form">
<?= $form->field($contact, 'name')->textInput(['placeholder' => "Name"])->label(false) ?>
<?= $form->field($contact, 'phone')->textInput(['placeholder' => "Contact Number"])->label(false) ?>
<?= $form->field($contact, 'email')->textInput(['placeholder' => "Email Address"])->label(false) ?>
<?= $form->field($contact, 'type')
        ->dropDownList(['RESIDENTIAL HOME' => 'RESIDENTIAL HOME','RESIDENTIAL DEVELOPMENT' => 'RESIDENTIAL DEVELOPMENT','COMMERCIAL ENTERPRISES' => 'COMMERCIAL ENTERPRISES','COMMERCIAL AGRICULTURE' => 'COMMERCIAL AGRICULTURE','NOT SURE' => 'NOT SURE'],['prompt' => 'Please indicate what type of premises you require solar for?','class' => 'form-control select-arrow'])->label(false);
?>
<?= $form->field($contact, 'size')
        ->dropDownList(['SINGLE PHASE' => 'SINGLE PHASE','THREE PHASE' => 'THREE PHASE','NOT SURE' => 'NOT SURE'],['prompt' => 'What is your Breaker Size?','class' => 'form-control select-arrow'])->label(false);
?>
</div>
<div class="col-sm-12 contact-form">
<?= $form->field($contact, 'body')->textarea(['rows' => 4,'class' => 'form-control text-area','placeholder' => 'Please provide any other details to help us an accurate quote:'])->label(false); ?>
</div>
<div class="col-lg-7 col-sm-12 indication-text">
We will contact you within one business day.
</div>
<div class="col-lg-5 col-sm-12 submit-area">
<div class="form-group">
<?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn-lg', 'name' => 'contact-button', 'data-animation' => 'animated lightSpeedIn']) ?>
</div>
</div>
</div>
<?php ActiveForm::end(); ?>
</div>
<div class="col-lg-6 col-sm-12 col-md-12 address-map-area">
<div class="row">
<div class="col-sm-12 contact-person">
<h4><span><i class="fas fa-phone-volume"></i></span> &nbsp; Get in Touch</h4>
<h3><a href="tel:<?=Yii::$app->settings->custom('get-in-touch')?>"><?=Yii::$app->settings->custom('get-in-touch')?></a></h3>
</div>
<div class="col-sm-12 address-person">
<div class="row justify-content-md-center">
<div class="col-lg-12 col-sm-12">
<div class="address-1">
<p><?=nl2br(Yii::$app->settings->custom('office-address'))?><br><a href="tel:<?=Yii::$app->settings->custom('office-phone')?>">Tel - <?=Yii::$app->settings->custom('office-phone')?></a><br><a href="mailto:<?=Yii::$app->settings->custom('office-email')?>">Email - <?=Yii::$app->settings->custom('office-email')?></a></p>
</div>
</div>
</div>
<div class="col-sm-12 contact-person">
<h4><span><i class="far fa-compass"></i></span> &nbsp; Head Office</h4>
<div class="col-lg-12 col-sm-12">
<div class="address-2">
<p><?=nl2br(Yii::$app->settings->custom('head-office-address'))?> <br> <a href="https://<?=Yii::$app->settings->custom('head-office-web')?>" target="_blank"><?=Yii::$app->settings->custom('head-office-web')?></a><br><a href="tel:<?=Yii::$app->settings->custom('head-office-phone')?>">Tel - <?=Yii::$app->settings->custom('head-office-phone')?></a></p>
</p>
</div>
</div>
</div>
</div>
<div class="col-lg-12 col-sm-12 map-area no-padding">
<div id="googleMap" style="width:100%;height:100%"></div>
</div>
</div>
</div>
</div>
</div>
<div id="section2"></div>
</section>
<section class="solution-area">
<div class="container-fluid custome-fluid">
<div class="row">
<div class="col-sm-12 center-head" data-scrollreveal="enter top over 2s after 0.5s">
<?=\frontend\widgets\Block::widget(['id' => 4])?>
</div>
<div class="col-sm-12 solution-landing">
<div class="row justify-content-md-center">
<div class="col-sm-5 hvr-float link-solutions" data-scrollreveal="enter top over 1s after 0.5s">
<a href="solutions-residential.html">
<div class="residential">
<img src="<?=$directoryAsset ?>/images/icon-2.png" alt="home">
<?=\frontend\widgets\Block::widget(['id' => 5])?>
</div>
</a>
</div>
<div class="col-sm-5 hvr-float link-solutions" data-scrollreveal="enter top over 1s after 0.5s">
<a href="solutions-commercial.html">
<div class="commercial">
<img src="<?=$directoryAsset ?>/images/icon-1.png" alt="home">
<?=\frontend\widgets\Block::widget(['id' => 6])?>
</div>
</a>
</div>
</div>
</div>
</div>
</div>
</section>