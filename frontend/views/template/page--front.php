<?php
/* @var $this yii\web\View */
$this->title = 'Soleco | Home';
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
use yii\helpers\Html;
use common\helpers\Url;
?>
<section class="carausel-area cd-section">
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
<?=\frontend\widgets\Banners::widget()?>
</div>
<div class="navbar-area" id="fixedmenu">
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="cd-vertical-nav">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
<div class="navbar-nav">
<a class="nav-item nav-link" href="#section1">INTRO <span class="sr-only">(current)</span></a>
<a class="nav-item nav-link" href="#section2">ADVANTAGES</a>
<a class="nav-item nav-link" href="#section6">OUR PORTFOLIO</a>
<a class="nav-item nav-link" href="#section3">WHY SOLECO </a>
<a class="nav-item nav-link" href="#section4">TESTIMONIALS</a>
<a class="nav-item nav-link" href="#section5">PARTNERS</a>
</div>
</div>
</nav>
</div>
<div id="section1"></div>
</section>
<section class="welcome-area">
<div class="container-fluid custome-fluid">
<div class="row">
<div class="col-sm-12 col-lg-4 carausel-link d-sm-block d-xs-block d-xl-none d-md-none d-lg-none">
<div class="row">
<div class="col-sm-6 hvr-float" data-scrollreveal="enter top over 1s after 0.5s">
<div class="residential">
<a href="#">
<img src="<?=$directoryAsset ?>/images/icon-2.png" alt="home">
<h5>Residential</h5>
<h6>Solar Solutions</h6>
</a>
</div>
</div>
<div class="col-sm-6 hvr-float" data-scrollreveal="enter top over 1s after 0.5s">
<div class="commercial">
<a href="#">
<img src="<?=$directoryAsset ?>/images/icon-1.png" alt="home">
<h5>Commercial</h5>
<h6>Solar Solutions</h6>
</a>
</div>
</div>
</div>
</div>
<div class="col-sm-12 col-lg-4 welcome-text" data-scrollreveal="enter top over 2s after 0.5s">
<?=\frontend\widgets\Block::widget(['id' => 1])?>
</div>
<div class="col-sm-12 col-lg-3 services-area">
<div class="row">
<div class="col-sm-6 hvr-float service-items" data-scrollreveal="enter left over 2s after 0.5s">
<?=\frontend\widgets\Block::widget(['id' => 8])?>
</div>
<div class="col-sm-6 hvr-float service-items" data-scrollreveal="enter right over 2s after 0.5s">
<?=\frontend\widgets\Block::widget(['id' => 9])?>
</div>
<div class="col-sm-6 hvr-float service-items" data-scrollreveal="enter left over 2s after 0.5s">
<?=\frontend\widgets\Block::widget(['id' => 10])?>
</div>
<div class="col-sm-6 hvr-float service-items" data-scrollreveal="enter right over 2s after 0.5s">
<?=\frontend\widgets\Block::widget(['id' => 11])?>
</div>
<div class="col-sm-6 service-items col-centered" data-scrollreveal="enter left over 2s after 0.5s">
<?=\frontend\widgets\Block::widget(['id' => 12])?>
</div>
</div>
</div>
<div class="col-sm-12 col-lg-5 welcome-bg" data-scrollreveal="enter bottom over 2s after 0.5s">
<img src="<?=Yii::$app->imagemanager->getImagePath(Yii::$app->settings->img('welcome'),508,382)?>" alt="Welcome to Soleco">
</div>
</div>
</div>
<div id="section2"></div>
</section>
<section class="welcome-area advantage-area">
<div class="container-fluid custome-fluid">
<div class="row justify-content-md-center">
<div class="col-sm-12 center-head" data-scrollreveal="enter top over 2s after 0.5s">
<?=\frontend\widgets\Block::widget(['id' => 3])?>
</div>
<div class="col-sm-12 col-lg-7 services-area">
<div class="row">
<div class="col-sm-3 hvr-float service-items" data-scrollreveal="enter left over 2s after 0.5s">
<?=\frontend\widgets\Block::widget(['id' => 13])?>
</div>
<div class="col-sm-3 hvr-float service-items" data-scrollreveal="enter right over 2s after 0.5s">
<?=\frontend\widgets\Block::widget(['id' => 14])?>
</div>
<div class="col-sm-3 hvr-float service-items" data-scrollreveal="enter left over 2s after 0.5s">
<?=\frontend\widgets\Block::widget(['id' => 15])?>
</div>
<div class="col-sm-3 hvr-float service-items" data-scrollreveal="enter right over 2s after 0.5s">
<?=\frontend\widgets\Block::widget(['id' => 16])?>
</div>
</div>
</div>
</div>
</div>
<div id="section6"></div>
</section>
<section class="solution-area">
<div class="container-fluid custome-fluid">
<div class="row">
<div class="col-sm-12 center-head" data-scrollreveal="enter top over 2s after 0.5s">
<?=\frontend\widgets\Block::widget(['id' => 4])?>
</div>
<div class="col-sm-12 solution-landing">
<div class="row justify-content-md-center">
<div class="col-sm-5 hvr-float link-solutions" data-scrollreveal="enter top over 1s after 0.5s">
<a href="solutions-residential.html">
<div class="residential">
<img src="<?=$directoryAsset ?>/images/icon-2.png" alt="home">
<?=\frontend\widgets\Block::widget(['id' => 5])?>
</div>
</a>
</div>
<div class="col-sm-5 hvr-float link-solutions" data-scrollreveal="enter top over 1s after 0.5s">
<a href="solutions-commercial.html">
<div class="commercial">
<img src="<?=$directoryAsset ?>/images/icon-1.png" alt="home">
<?=\frontend\widgets\Block::widget(['id' => 6])?>
</div>
</a>
</div>
</div>
</div>
</div>
</div>
<div id="section3"></div>
</section>
<section class="why-us-area">
<div class="container-fluid custome-fluid">
<div class="row">
<div class="col-lg-5 col-sm-12 why-us" data-scrollreveal="enter left over 2s after 0.5s">
<div class="col-sm-12 left-head">
<?=\frontend\widgets\Block::widget(['id' => 7])?>
</div>
</div>
<div class="col-lg-7 col-sm-12 spec-area">
<div class="row">
<div class="col-lg-3 col-sm-12 specification">
<div class="row">
<div class="col-lg-12 col-sm-6 hvr-float spec-items" data-scrollreveal="enter left over 2s after 0.1s">
<?=\frontend\widgets\Block::widget(['id' => 17])?>
</div>
<div class="col-lg-12 col-sm-6 hvr-float spec-items" data-scrollreveal="enter left over 2s after 0.2s">
<?=\frontend\widgets\Block::widget(['id' => 18])?>
</div>
<div class="col-lg-12 col-sm-6 hvr-float spec-items" data-scrollreveal="enter left over 2s after 0.2s">
<?=\frontend\widgets\Block::widget(['id' => 19])?>
</div>
</div>
</div>
<div class="col-sm-12 col-lg-6 spec-image" data-scrollreveal="enter bottom over 2s after 0.1s">
<img src="<?=Yii::$app->imagemanager->getImagePath(Yii::$app->settings->img('why-soleco'),361,366)?>" alt="Soleco Specialities" class="eco-image">
<div class="row justify-content-md-center">
<div class="col-lg-5 col-sm-6 hvr-float spec-items" data-scrollreveal="enter left over 2s after 0.2s">
<?=\frontend\widgets\Block::widget(['id' => 20])?>
</div>
</div>
</div>
<div class="col-lg-3 col-sm-12 specification">
<div class="row">
<div class="col-lg-12 col-sm-6 hvr-float spec-items" data-scrollreveal="enter right over 2s after 0.1s">
<?=\frontend\widgets\Block::widget(['id' => 21])?>
</div>
<div class="col-lg-12 col-sm-6 hvr-float spec-items" data-scrollreveal="enter right over 2s after 0.2s">
<?=\frontend\widgets\Block::widget(['id' => 22])?>
</div>
<div class="col-lg-12 col-sm-6 hvr-float spec-items" data-scrollreveal="enter right over 2s after 0.2s">
<?=\frontend\widgets\Block::widget(['id' => 23])?>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div id="section4"></div>
</section>
<section class="client-testimonial-area">
<div class="container-fluid custome-fluid">
<div class="row">
<div class="col-sm-12 center-head" data-scrollreveal="enter top over 2s after 0.5s">
<h2>WHAT <span>CLIENTS SAY</span></h2>
</div>
<?=\frontend\widgets\Testimonials::widget()?>
</div>
</div>
<div id="section5"></div>
</section>
<section class="clients-area">
<div class="container-fluid custome-fluid">
<div class="row">
<div class="col-sm-12 left-head no-padding" data-scrollreveal="enter left over 2s after 0.5s">
<h2>OUR <span>partners</span></h2>
</div>
<?=\frontend\widgets\Partners::widget()?>
</div>
</div>
</section>
