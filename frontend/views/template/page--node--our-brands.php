<?php
use common\helpers\Url;
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
$first = true;
$this->title = 'Hero Paints | '.$model->title;
?>
<section>
<div class="inner-bg">
</div>
<div class="slider-overlay">
<div class="breadcrum-area">
<div class="container">
<div class="row">
<div class="col-sm-6 page-title">
<h2><?=$model->title?></h2>
</div>
<div class="col-sm-6 site-breadcrumb">
<ul class="list-inline">
<li class="list-inline-item home-link"><a href="<?=Url::home()?>">Home</a></li>
<li class="list-inline-item sepration">/</li>
<li class="list-inline-item"><?=$model->title?></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</section>
<section>
<div class="container">
<div class="row">
<div class="col-sm-12 brand-area-headding">
<h2><?=$model->title?></h2>
<?=$model->description?>
</div>
<?php foreach ($model->nodeImages as $img):?>
<div class="col-sm-4">
<div class="logo-area">
<a href="<?=$img->node_image_description?>"><img src="<?=Yii::getAlias('@url').'/nodeimages/'.$img->node_image?>" alt="<?=$img->node_image_title?>">
<h3><?=$img->node_image_title?></h3></a>
</div>
</div>
<?php endforeach; ?>
</div>
</div>
</section>

