<?php
use yii\helpers\Html;
use common\helpers\Url;
use yii\bootstrap\ActiveForm;
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
$contact = new \frontend\models\ContactForm();
$this->title = 'Soleco | '.$model->title;
$res1 = \backend\models\SolarSolution::findOne(14);
$res2 = \backend\models\SolarSolution::findOne(15);
$f1 = $f2 = true;
?>
<section class="inner-bg1 cd-section">
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
<div class="carousel-inner">
<div class="carousel-item active">
<img class="d-block w-100" src="<?=$directoryAsset?>/images/bg7.jpg" alt="First slide">
<div class="carousel-caption">
<div class="row justify-content-md-center">
<div class="col-sm-7 carausel-text inner-text">
<div class="page-title">
<h2><?=$model->title?></h2>
<p><?=$model->summary?></p>
</div>
<div class="breadcrumb-area">
<ul class="list-inline">
<li class="list-inline-item"><a href="<?=Url::home()?>"><i class="fas fa-home"></i></a></li>
<li class="list-inline-item seperation">/</li>
<li class="list-inline-item"><?=$model->title?></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="navbar-area" id="fixedmenu">
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="cd-vertical-nav">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
<div class="navbar-nav">
<a class="nav-item nav-link" href="#section1">Individual Homes <span class="sr-only">(current)</span></a>
<a class="nav-item nav-link" href="#section2">Project Developers</a>
</div>
</div>
</nav>
</div>
<div id="section1"></div>
</section>

<section class="solution-section" data-scrollreveal="enter top over 2s after 0.5s">
<?php if(isset($res1)) : ?>
<div class="container-fluid custome-fluid">
<div class="row justify-content-md-center">
<div class="col-lg-10 col-sm-12 center-head inner-solution-head">
<h2><span><?=$res1->title?></span></h2>
<?=$res1->description?>
<hr>
<div class="col-sm-12 head-list">
<div class="row">
<?php
$lists = explode(',',$res1->tags);
foreach ($lists as $list):
?>
<div class="col-lg-4 col-sm-6 hvr-float service-items" data-scrollreveal="enter left over 2s after 0.5s">
<div class="services">
<a href="<?=Url::node('/node/9')?>">
<div class="row">
<div class="col-sm-4">
<img src="<?=$directoryAsset?>/images/logo-icon.png" alt="icon">
</div>
<div class="col-sm-8 list-setails no-padding">
<h5><span><?=$list?></span></h5>
</div>
</div>
</a>
</div>
</div>
<?php endforeach; ?>
</div>
</div>
</div>
<div class="col-lg-5 col-sm-12 operation-bg no-padding">
<div id="carouselExampleControls2" class="carousel slide" data-ride="carousel">
<div class="carousel-inner">
<?php 
if(isset($res1->nodeImages)): 
foreach ($res1->nodeImages as $rimg1):
    if($f1): 
        $f1 = false; 
?>
<div class="carousel-item active">
<img src="<?=Yii::getAlias('@url').'/nodeimages/'.$rimg1->node_image?>" alt="<?=$rimg1->node_image_title?>">
</div>
<?php else: ?>
<div class="carousel-item">
<img src="<?=Yii::getAlias('@url').'/nodeimages/'.$rimg1->node_image?>" alt="<?=$rimg1->node_image_title?>">
</div>
<?php endif; ?>
<?php endforeach;?>
<?php endif; ?>
</div>
<a class="carousel-control-prev" href="#carouselExampleControls2" role="button" data-slide="prev">
<img src="<?=$directoryAsset?>/images/arrow-left.png" alt="arrow">
</a>
<a class="carousel-control-next" href="#carouselExampleControls2" role="button" data-slide="next">
<img src="<?=$directoryAsset?>/images/arrow-right.png" alt="arrow">
</a>
</div>
</div>
<div class="col-lg-7 col-sm-12 operation-text solution-inner-text">
<div class="operation-details">
<div class="row justify-content-md-center">
<div class="col-lg-10 col-sm-12 left-head">
<?=$res1->other?>
<a class="btn btn-success" href="#inline" data-lity>Enquire Now</a>
</div>
</div>
</div>
</div>
</div>
</div>
<?php endif;?>
<div id="section2"></div>
</section>
<section class="agriculture-section" data-scrollreveal="enter top over 2s after 0.5s">
<?php if(isset($res2)) : ?>
<div class="container-fluid custome-fluid">
<div class="row justify-content-md-center">
<div class="col-lg-10 col-sm-12 center-head inner-solution-head">
<h2><?=$res2->title?></h2>
<?=$res2->description?>
<hr>
</div>
<div class="col-lg-7 col-sm-12 operation-text solution-inner-text2">
<div class="operation-details">
<div class="row justify-content-md-center">
<div class="col-lg-10 col-sm-12 left-head right-area">
<?=$res2->other?>
<a class="btn btn-success" href="#inline" data-lity>Enquire Now</a>
</div>
</div>
</div>
</div>
<div class="col-lg-5 col-sm-12 operation-bg no-padding">
<div id="carouselExampleControls3" class="carousel slide" data-ride="carousel">
<div class="carousel-inner">
<?php 
if(isset($res2->nodeImages)): 
foreach ($res2->nodeImages as $rimg2):
    if($f2): 
        $f2 = false; 
?>
<div class="carousel-item active">
<img src="<?=Yii::getAlias('@url').'/nodeimages/'.$rimg2->node_image?>" alt="<?=$rimg2->node_image_title?>">
</div>
<?php else: ?>
<div class="carousel-item">
<img src="<?=Yii::getAlias('@url').'/nodeimages/'.$rimg2->node_image?>" alt="<?=$rimg2->node_image_title?>">
</div>
<?php endif; ?>
<?php endforeach;?>
<?php endif; ?>
</div>
<a class="carousel-control-prev" href="#carouselExampleControl3" role="button" data-slide="prev">
<img src="<?=$directoryAsset?>/images/arrow-left.png" alt="arrow">
</a>
<a class="carousel-control-next" href="#carouselExampleControls3" role="button" data-slide="next">
<img src="<?=$directoryAsset?>/images/arrow-right.png" alt="arrow">
</a>
</div>
</div>
</div>
</div>
<?php endif;?>
</section>
<section class="go-back-area">
<div class="container-fluid custome-fluid">
<div class="row">
<div class="col-sm-12 goback-btn">
<a class="btn btn-success" href="<?=Url::node('/node/19')?>">Go to COMMERCIAL SOLAR</a>
</div>
</div>
</div>
</section>