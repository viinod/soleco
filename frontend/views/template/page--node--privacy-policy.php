<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = 'My Farm Trip | Privacy Policy';
?>
<?php $this->beginBlock('header'); ?>
<div class="container inner-header">
<div class="row">
<div class="col-sm-7 header-description">
<h2><?=$model->title?></h2>
<ul class="list-inline">
<li class="list-inline-item"><a href="<?=Url::home()?>"><i class="fas fa-home"></i></a></li>
<li class="list-inline-item seperation">/</li>
<li class="list-inline-item"><?=$model->title?></li>
</ul>
</div>
<div class="col-sm-5 header-video">
<h2><?=Yii::t('*', 'header_how_it')?></h2>
<a href="<?=Yii::$app->settings->custom('youtube-link')?>" data-lity>
<img src="<?=Yii::$app->imagemanager->getImagePath(Yii::$app->settings->img('youtube-image'),600,321)?>" alt="youtube">
</a>
</div>
</div>
</div>
<?php $this->endBlock(); ?>
<section class="features-section about-section">
<div class="container">
<div class="row">
<div class="col-sm-12 features-head">
<img src="images/head-dots.png" alt="dots">
<p><?=$model->summary?></p>
<h3><?=$model->title?></h3>
</div>
<div class="col-sm-12 about-text about-main">
<?=$model->description?>
</div>
</div>
</div>
</section>