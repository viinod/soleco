<?php

/* @var $this yii\web\Views */

use yii\helpers\Html;
$baseUrl = Yii::getAlias('@web');
$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clearfix"></div>
<section class="breadcrumb-bg">
	<div class="container">
    	<div class="row">
        	<div class="col-sm-6 page-title">
            </div><!--close page-title-->
                    <div class="col-sm-6 breadcrumb-area">
        	<ol class="breadcrumb">
              <li><a href="<?=Yii::$app->homeUrl;?>">Home</a></li>
              <li class="active"><?=$model->title?></li>
            </ol>
        </div><!--close breadcrumb-area-->
        </div><!--close row-->
    </div><!--close container-->
</section>
<section class="service-bg">
	<div class="container">
    	<div class="row">
        <h1><?=$model->title?></h1>
        <hr>
			<?php if(Yii::$app->session->hasFlash('success')){ ?>
<div class="alert alert-success alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<?= Yii::$app->session->getFlash('success'); ?>
</div>
<?php } ?>
        	<div class="col-sm-12 detail-area">
        	<?php if(!empty($model->image)): ?>
            	<img src="<?=Yii::getAlias('@siteUrl').'/'.\yii\helpers\Inflector::pluralize($model->type).'/'.$model->image?>" alt="...">
            <?php endif; ?>
        	<?php if(!empty($model->description)): ?>
            	<?=$model->description?>
            <?php endif; ?>       

        </div><!--close detail-img-->
        <?php if(count($model->nodeImages) > 0): ?>
        <div class="col-sm-12 related-images">
        <h4>related images</h4>
<?= \frontend\widgets\AdvancedImages::widget(['image' => $model->nodeImages]) ?>
        </div>
        
        </div>
<?php endif;?>
        </div><!--close row-->
    </div><!--close container-->
</section><!--close service-bg-->