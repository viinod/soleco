<?php
use common\helpers\Url;
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
$first = true;
$this->title = 'Hero Paints | '.$model->title;
?>
<section>
<div class="inner-bg">
</div>
<div class="slider-overlay">
<div class="breadcrum-area">
<div class="container">
<div class="row">
<div class="col-sm-6 page-title">
<h2><?=$model->title?></h2>
</div>
<div class="col-sm-6 site-breadcrumb">
<ul class="list-inline">
<li class="list-inline-item home-link"><a href="<?=Url::home()?>">Home</a></li>
<li class="list-inline-item sepration">/</li>
<li class="list-inline-item"><?=$model->title?></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</section>
<section>
<div class="container">
<div class="row">
<?php foreach ($model->nodeImages as $img):?>
<div class="col-sm-4 awards-details awards-detail-inner">
<div class="sisters-concern">
<a href="<?=Yii::getAlias('@url').'/nodeimages/'.$img->node_image?>" data-lity data-lity-desc="<?=$img->node_image_title?>">
<img src="<?=Yii::getAlias('@url').'/nodeimages/'.$img->node_image?>" alt="<?=$img->node_image_title?>"></a>
<h2><?=$img->node_image_title?></h2>
<p><?=$img->node_image_description?></p>
<h3><i class="fas fa-phone-volume"></i> &nbsp;<?=$img->text1?></h3>
<h3><i class="fas fa-envelope"></i> &nbsp; <?=$img->text2?></h3>
</div>
</div>
<?php endforeach; ?>
</div>
</div>
</section>