<?php
use yii\helpers\Html;
use common\helpers\Url;
use yii\bootstrap\ActiveForm;
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
$basic = \backend\models\Basic::findOne(11);
$careers =  \backend\models\Career::find()->where(['type' => 'career','status' => 1])->all();
$i = 1;
$this->title = 'Soleco | Careers';
?>
<section class="inner-bg1 cd-section">
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
<div class="carousel-inner">
<div class="carousel-item active">
<img class="d-block w-100" src="<?=$directoryAsset?>/images/solutions.jpg" alt="First slide">
<div class="carousel-caption">
<div class="row justify-content-md-center">
<div class="col-lg-8 col-sm-12 carausel-text inner-text">
<div class="page-title">
<h2>Careers</h2>
<p><?=$basic->summary?></p>
</div>
<div class="breadcrumb-area">
<ul class="list-inline">
<li class="list-inline-item"><a href="<?=Url::home()?>"><i class="fas fa-home"></i></a></li>
<li class="list-inline-item seperation">/</li>
<li class="list-inline-item">Careers</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="mission-area job-area">
<div class="container-fluid">
<div class="row justify-content-md-center">
<div class="col-sm-12">
<div class="row justify-content-md-center">
<div class="col-sm-9" data-scrollreveal="enter top over 2s after 0.5s">
<div class="row">
<div class="col-sm-12 career-details careers-inner no-padding">
<h2><?=$model->title?></h2>
<ul class="list-inline">
<li class="list-inline-item"><span>Post : </span><?=$model->field1?></li>
<li class="list-inline-item"><span>Qualifications : </span><?=$model->field2?></li>
<li class="list-inline-item"><span>Experience : </span><?=$model->summary?></li>
<li class="list-inline-item"><span>Job Location : </span><?=$model->field3?></li>
</ul>
<h3>Job Profile</h3>
<div class="jobprofile">
<?=$model->description?>
</div>
<p><?=Yii::t('*', 'career_message')?><a href="mailto:<?=Yii::$app->settings->custom('career-mail')?>" target="_top"><?=Yii::$app->settings->custom('career-mail')?></a></p>
<br><br>
<div class="col-sm-12 goback-btn">
<a class="btn btn-success" href="<?=Url::node('/node/11')?>">Go to CAREERS</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="mail-area">
<div class="container-fluid">
<div class="row">
<div class="col-sm-12 mail-to-us">
<h2>Can't find your job</h2>
<p>We appreciate your interest in exploring career opportunities with us. We are looking for talented professionals to join our team. Feel free to share your CV</p>
<a href="mailto:admin@soleco.in" target="_top"><i class="far fa-arrow-alt-circle-right"></i>Post Now</a>
</div>
</div>
</div>
</section>
