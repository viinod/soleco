<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
$baseUrl = Yii::getAlias('@web');
$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clearfix"></div>
<section class="breadcrumb-bg">
	<div class="container">
    	<div class="row">
        	<div class="col-sm-6 page-title">
            	<h2>services</h2>
            </div><!--close page-title-->
                    <div class="col-sm-6 breadcrumb-area">
        	<ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">Services</li>
            </ol>
        </div><!--close breadcrumb-area-->
        </div><!--close row-->
    </div><!--close container-->
</section>
<section class="service-bg">
	<div class="container">
    	<div class="row">
        <h1><?=$model->title?></h1>
        <hr>
        	<div class="col-sm-12 detail-area">
        	<?php if(!empty($model->image)): ?>
            	<img src="<?=$baseUrl?>/images/detail-img.jpg" alt="...">
            <?php endif; ?>
        	<?php if(!empty($model->description)): ?>
            	<?=$model->description?>
            <?php endif; ?>       

        </div><!--close detail-img-->
        <div class="col-sm-12 related-images">
        <h4>related images</h4>
        
        
        <div id="carousel-example-generic7" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic7" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic7" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic7" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
            <div class="col-sm-3 rel-img">
            	<img src="<?=$baseUrl?>/images/rel-img-1.jpg" alt="...">
            </div><!--close rel-img-title-->
            
            <div class="col-sm-3 rel-img">
            	<img src="<?=$baseUrl?>/images/rel-img-2.jpg" alt="...">
            </div><!--close rel-img-title-->
            
            <div class="col-sm-3 rel-img">
            	<img src="<?=$baseUrl?>/images/rel-img-3.jpg" alt="...">
            </div><!--close rel-img-title-->	
            
           <div class="col-sm-3 rel-img">
            	<img src="<?=$baseUrl?>/images/rel-img-4.jpg" alt="...">
            </div><!--close rel-img-title-->
    </div>
    <div class="item">
            <div class="col-sm-3 rel-img">
            	<img src="<?=$baseUrl?>/images/rel-img-1.jpg" alt="...">
            </div><!--close rel-img-title-->
            
            <div class="col-sm-3 rel-img">
            	<img src="<?=$baseUrl?>/images/rel-img-2.jpg" alt="...">
            </div><!--close rel-img-title-->
            
            <div class="col-sm-3 rel-img">
            	<img src="<?=$baseUrl?>/images/rel-img-3.jpg" alt="...">
            </div><!--close rel-img-title-->	
            
           <div class="col-sm-3 rel-img">
            	<img src="<?=$baseUrl?>/images/rel-img-4.jpg" alt="...">
            </div><!--close rel-img-title-->
    </div>
    
    <div class="item">
            <div class="col-sm-3 rel-img">
            	<img src="<?=$baseUrl?>/images/rel-img-1.jpg" alt="...">
            </div><!--close rel-img-title-->
            
            <div class="col-sm-3 rel-img">
            	<img src="<?=$baseUrl?>/images/rel-img-2.jpg" alt="...">
            </div><!--close rel-img-title-->
            
            <div class="col-sm-3 rel-img">
            	<img src="<?=$baseUrl?>/images/rel-img-3.jpg" alt="...">
            </div><!--close rel-img-title-->	
            
           <div class="col-sm-3 rel-img">
            	<img src="<?=$baseUrl?>/images/rel-img-4.jpg" alt="...">
            </div><!--close rel-img-title-->
    </div>
  </div>


</div>
        
        </div>
        </div><!--close row-->
    </div><!--close container-->
</section><!--close service-bg-->