<div class="services2" data-toggle="tooltip" data-placement="top" title="<?=$model->description?>">
<a href="<?=$model->url?>">
<img src="<?=Yii::$app->imagemanager->getImagePath($model->icon)?>" alt="<?=$model->title?>">
<h5><?=$model->title?><span><?=$model->sub_title?></span></h5>
</a>
</div>