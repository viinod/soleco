<div class="services" data-toggle="tooltip" data-placement="top" title="<?=$model->description?>">
<a href="<?=$model->url?>">
<img src="<?=Yii::$app->imagemanager->getImagePath($model->icon)?>" alt="<?=$model->title?>">
<h5><span><?=$model->title?></span></h5>
</a>
</div>