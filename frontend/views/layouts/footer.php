<?php
use common\helpers\Url;
?>
<footer class="footer-bg">
<div class="footer-comp-details">
<div class="container-fluid custome-fluid">
<div class="row">
<div class="col-sm-12 footer-text">
<p><?=Yii::t('*', 'footer_note')?></p>
</div>
</div>
</div>
</div>
<div class="container-fluid custome-fluid">
<div class="row">
<div class="col-lg-6 col-sm-12 footer-logo">
<a href="<?=Url::home()?>"><img src="<?=Yii::$app->imagemanager->getImagePath(Yii::$app->settings->img('logo'),232,93)?>" alt="logo"></a>
</div>
<div class="col-lg-6 col-sm-12 footer-details">
<div class="row">
<div class="col-sm-6">
<div class="office-address">
<p><?=nl2br(Yii::$app->settings->custom('office-address'))?><br><a href="tel:<?=Yii::$app->settings->custom('office-phone')?>">Tel - <?=Yii::$app->settings->custom('office-phone')?></a><br><a href="mailto:<?=Yii::$app->settings->custom('office-email')?>">Email - <?=Yii::$app->settings->custom('office-email')?></a></p>
</div>
<div class="office-address">
<h4><span><i class="far fa-compass"></i></span> Head Office</h4>
<p><?=nl2br(Yii::$app->settings->custom('head-office-address'))?> <br> <a href="https://<?=Yii::$app->settings->custom('head-office-web')?>" target="_blank"><?=Yii::$app->settings->custom('head-office-web')?></a><br><a href="tel:<?=Yii::$app->settings->custom('head-office-phone')?>">Tel - <?=Yii::$app->settings->custom('head-office-phone')?></a></p>
</div>
</div>
<div class="col-sm-6 copyright-area">
<div class="contact-number">
<h4><span><i class="fas fa-phone-volume"></i></span> Get in Touch</h4>
<h3><a href="tel:<?=Yii::$app->settings->custom('get-in-touch')?>"><?=Yii::$app->settings->custom('get-in-touch')?></a></h3>
</div>
<div class="social-media">
<ul class="list-inline">
<li class="list-inline-item"><a target="_blank" href="<?=Yii::$app->settings->custom('facebook')?>"><i class="fab fa-facebook-square"></i></a></li>
<li class="list-inline-item"><a target="_blank" href="<?=Yii::$app->settings->custom('twitter')?>"><i class="fab fa-twitter-square"></i></a></li>
<li class="list-inline-item"><a target="_blank" href="<?=Yii::$app->settings->custom('instagram')?>"><i class="fab fa-instagram"></i></a></li>
<li class="list-inline-item"><a href="mailto:<?=Yii::$app->settings->custom('email')?>"><i class="fas fa-envelope-open"></i></a></li>
<li class="list-inline-item"><a target="_blank" href="https://api.whatsapp.com/send?phone=<?=Yii::$app->settings->custom('whatsapp')?>&amp;text=<?=Yii::$app->settings->custom('whatsapp-text')?>" class="applyinfo-btn" target="_blank"><i class="fab fa-whatsapp-square"></i></a></li>
</ul>
</div>
<div class="copyright-text">
<p><?=Yii::t('*', 'copyright', time())?></p>
</div>
<div class="copyright-text">
<p>Powered by <a href="https://www.opesmount.com/" target="_blank"><img src="<?=$directoryAsset ?>/images/PB-logo.png" alt="logo"></a></p>
</div>
</div>
</div>
</div>
</div>
</div>
</footer>