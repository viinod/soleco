<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
AppAsset::register($this);
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta charset="<?= Yii::$app->charset ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php $this->registerCsrfMetaTags() ?>
<title><?= Html::encode($this->title) ?></title>
<?php $this->head() ?>  
</head>
<body>
<?php $this->beginBody() ?>
<?php $this->beginBody() ?>
<?= $this->render(
    'header.php',
    ['directoryAsset' => $directoryAsset]
) ?>
        <?= $content ?>
<?= $this->render(
    'footer.php',
    ['directoryAsset' => $directoryAsset]
) ?>

<?php $this->endBody() ?>

   	  	<script>
		$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		})
	</script>
   	<script>
		$('.owl-1').owlCarousel({
		loop:true,
		margin:50,
		responsiveClass:true,
		dots: true,
		responsive:{

			0:{
				items:1,
				nav:false,
				dots: true,
			},
			600:{
				items:1,
				nav:false,
				dots: true,
			},
			1000:{
				items:3,
				nav:false,
				loop:true,
				autoplay:true,
				dots: true,
			}
		}

		})
		
		$('.owl-2').owlCarousel({
		loop:true,
		margin:30,
		responsiveClass:true,
		dots: true,
		responsive:{

			0:{
				items:1,
				nav:false,
				dots: true,
			},
			600:{
				items:1,
				nav:false,
				dots: true,
			},
			1000:{
				items:2,
				nav:false,
				loop:true,
				autoplay:true,
				dots: true,
			}
		}

		})
		
		$('.owl-3').owlCarousel({
		loop:true,
		margin:30,
		responsiveClass:true,
		dots: true,
		responsive:{

			0:{
				items:3,
				nav:false,
				dots: false,
			},
			600:{
				items:3,
				nav:false,
				dots: true,
			},
			1000:{
				items:6,
				nav:false,
				loop:true,
				autoplay:true,
				dots: true,
			}
		}

		})
	</script>
	

<!-- MORE LESS CODE -->
<script>
	function myFunction() {
	  var dots = document.getElementById("dots1");
	  var moreText = document.getElementById("more1");
	  var btnText = document.getElementById("myBtn1");

	  if (dots.style.display === "none") {
		dots.style.display = "inline";
		btnText.innerHTML = "Read more"; 
		moreText.style.display = "none";
	  } else {
		dots.style.display = "none";
		btnText.innerHTML = "Read less"; 
		moreText.style.display = "inline";
	  }	
	}
	</script>
	<!-- MORE LESS CODE -->
		<script>
		$(document).ready(function() {
			//change the integers below to match the height of your upper dive, which I called
			//banner.  Just add a 1 to the last number.  console.log($(window).scrollTop())
			//to figure out what the scroll position is when exactly you want to fix the nav
			//bar or div or whatever.  I stuck in the console.log for you.  Just remove when
			//you know the position.
			$(window).scroll(function() {

				console.log($(window).scrollTop());

				if ($(window).scrollTop() > 780) {
					$('#fixedmenu').addClass('navbar-fixed');
				}

				if ($(window).scrollTop() < 781) {
					$('#fixedmenu').removeClass('navbar-fixed');
				}
			});
		});

	</script>
<script>
		$(document).ready(function() {
		
			$(window).scroll(function() {

				console.log($(window).scrollTop());

				if ($(window).scrollTop() > 10) {
					$('#fixedmenu2').addClass('navbar-fixed2');
				}

				if ($(window).scrollTop() < 11) {
					$('#fixedmenu2').removeClass('navbar-fixed2');
				}
			});
		});

	</script>
</body>
</html>
<?php $this->endPage() ?>
