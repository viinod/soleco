<?php
use yii\helpers\Html;
use common\helpers\Url;
$current = Url::current();
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
?>
<header class="header-bg cd-section" id="fixedmenu2">
<div class="container-fluid custome-fluid">
<div class="row">
<div class="col-sm-12 col-md-4 col-lg-3 logo">
<a href="<?=Url::home()?>"><img src="<?=Yii::$app->imagemanager->getImagePath(Yii::$app->settings->img('logo'),232,93)?>" alt="logo"></a>
</div>
<div class="col-sm-12 col-md-12 col-lg-5"></div>
<div class="col-sm-12 col-md-6 col-lg-3 watsapp-number">
<a href="https://api.whatsapp.com/send?phone=<?=Yii::$app->settings->custom('whatsapp')?>&amp;text=<?=Yii::$app->settings->custom('whatsapp-text')?>" class="applyinfo-btn" target="_blank"><span><i class="fab fa-whatsapp"></i> : </span><?=Yii::$app->settings->custom('whatsapp')?></a>
</div>
<div class="col-sm-12 col-md-2 col-lg-1">
<div class="menu-area">
<ul class="list-watsap list-unstyled d-sm-block d-xs-block d-xl-none d-md-none d-lg-none">
<a href="https://api.whatsapp.com/send?phone=<?=Yii::$app->settings->custom('whatsapp')?>&amp;text=<?=Yii::$app->settings->custom('whatsapp-text')?>" class="applyinfo-btn" target="_blank"><li><span><i class="fab fa-whatsapp"></i> : </span><?=Yii::$app->settings->custom('whatsapp')?></li></a>
</ul>
<button class="main-menu-indicator hvr-push" id="open-button">
<img src="<?=$directoryAsset ?>/images/menu-burger.png" alt="menu">
</button>
<div class="menu-wrap">
<div class="menu-content">
<div class="navigation">
<span class="pe-7s-close close-menu" id="close-button"></span>
</div>
<nav class="menu">
<div class="menu-list">
<ul>
<li class="menu-item-has-children"><a href="<?=Url::home()?>">Home </a>
</li>
<li><a href="<?=Url::node('/node/7')?>">About Us</a></li>
<li><a href="<?=Url::node('/node/9')?>">Solutions</a></li>
<li><a href="<?=Url::node('/node/13')?>">Services</a></li>
<li><a href="<?=Url::node('/node/8')?>">Contact Us</a></li>
<li><a href="<?=Url::node('/node/11')?>">Careers</a></li>
</ul>
</div>
</nav>
<div class="menu-information">
<ul>
<li><span><i class="fab fa-whatsapp"></i>:</span><a href="https://api.whatsapp.com/send?phone=<?=Yii::$app->settings->custom('whatsapp')?>&amp;text=<?=Yii::$app->settings->custom('whatsapp-text')?>" class="applyinfo-btn" target="_blank">+971 50-178-9943</a></li>
<li><span><i class="fas fa-phone"></i>:</span><a href="tel:<?=Yii::$app->settings->custom('phone')?>"><?=Yii::$app->settings->custom('phone')?></a></li>
<li><span><i class="fas fa-envelope"></i>:</span><a href="mailto:<?=Yii::$app->settings->custom('email')?>"><?=Yii::$app->settings->custom('email')?></a></li>
</ul>
<div class="social-media-area">
<ul class="list-inline">
<li class="list-inline-item"><a href="<?=Yii::$app->settings->custom('facebook')?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
<li class="list-inline-item"><a href="<?=Yii::$app->settings->custom('twitter')?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
<li class="list-inline-item"><a href="<?=Yii::$app->settings->custom('instagram')?>" target="_blank"><i class="fab fa-instagram"></i></a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</header>