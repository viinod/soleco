<?php
use yii\helpers\Html;
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
$first = true;
?>
<div class="col-sm-12 app-screen-head">
<img class="dots-head" src="<?=$directoryAsset ?>/images/head-dots.png" alt="dots">
<img src="<?=Yii::$app->imagemanager->getImagePath(Yii::$app->settings->img('logo-body'),275,102)?>" alt="logo">
<p><?=$model->summary?></p>
</div>
<div class="col-sm-12 mobile-splash-screens" data-scrollreveal="enter top over 2s after 0.5s">
<div class="mobile-slider2">
<div id="carouselExampleIndicators2" class="carousel slide carousel-fade" data-ride="carousel">
<div class="carousel-inner d-none d-xl-block d-md-block d-lg-block">
<?php foreach ($model->nodeImages as $image):?>
<?php if($first): $first = false; ?>
<div class="carousel-item active">
<img src="<?=Yii::getAlias('@url/nodeimages/').$image->node_image?>" alt="<?=$image->node_image_title?>">
</div>
<?php else: ?>
<div class="carousel-item">
<img src="<?=Yii::getAlias('@url/nodeimages/').$image->node_image?>" alt="<?=$image->node_image_title?>"></div>
<?php endif; ?>
<?php endforeach; ?>
</div>
<div class="carousel-inner d-md-none d-lg-none d-sm-block">
<div class="col-sm-12 carousel-item active">
<img src="<?=Yii::$app->imagemanager->getImagePath(7,354,453)?>" alt="slides">
</div>
<div class="col-sm-12 carousel-item">
<img src="<?=Yii::$app->imagemanager->getImagePath(8,354,453)?>" alt="slides">
</div>
</div>
</div>
</div>
</div>