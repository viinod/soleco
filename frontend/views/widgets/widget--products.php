<?php
use yii\helpers\Html;
use common\helpers\Url;
?>
<section class="our-product-area">
<div class="container">
<div class="row">
<div class="col-sm-12 products-head" data-scrollreveal="enter bottom over 2s after 0.5s">
<h2><span>Our</span> Products</h2>
</div>
<div class="col-sm-12 owl-carousel-2 owl-slides no-padding" data-scrollreveal="enter bottom over 2s after 1s">
<?php foreach ($models as $model):?>
<div class="item active">
<div class="row">
<div class="col-sm-12 product-details">
<figure><a href="#"><img src="<?=Yii::getAlias('@url').'/products/'.$model->image?>" alt="<?=$model->title?>"></a></figure>
<div class="product-title">
<h4><?=$model->title?></h4>
<a href="<?=Url::node('/node/'.$model->id)?>">Read More <i class="fas fa-arrow-circle-right"></i></a>
</div>
</div>
</div>
</div>
<?php endforeach;?>
</div>
</div>
</div>
</section>


               

              

            
