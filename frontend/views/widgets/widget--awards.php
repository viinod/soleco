<?php
use yii\helpers\Html;
use common\helpers\Url;
?>
<section class="awards-area">
<div class="container">
<div class="row">
<div class="col-lg-4 col-sm-12 our-achievements-text" data-scrollreveal="enter top over 2s after 0.5s">
<h2><span>Our</span>Achievements and awards</h2>
<?=\yii\helpers\StringHelper::truncate($model->description,170,'','UTF-8',true)?>
<a href="<?=Url::node('/node/4')?>">View More <i class="fas fa-arrow-circle-right"></i></a>
</div>
<div class="col-lg-8 col-sm-12 owl-carousel-1 owl-slides no-padding" data-scrollreveal="enter top over 4s after 1s">
<?php foreach ($model->nodeImages as $img):?>	
<div class="item active">
<div class="row">
<div class="col-sm-12 awards-details">
<figure><img src="<?=Yii::getAlias('@url').'/nodeimages/'.$img->node_image?>" alt="<?=$img->node_image_title?>"></figure>
<h2><?=$img->node_image_title?></h2>
<h3>Achieved on : <?=date('d-m-Y',$img->date)?></h3>
</div>
</div>
</div>
<?php endforeach;?>
</div>
</div>
</div>
</section>


               

              

            
