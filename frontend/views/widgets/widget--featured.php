<?php
use yii\helpers\Html;
$f1 = true;
$f2 = true;
?>
<div class="row">
<nav class="col-lg-3 col-xs-12 mainMenu">
<label for="toggleMenu" class="menuTitle" onclick="">&nbsp;</label>
<input type="checkbox" id="toggleMenu" />
<ul>
<div data-scrollreveal="enter left over 2s after 0.5s">
<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
<?php foreach ($models as $model):?>
<?php if($f1): $f1 = false; ?>
<a class="nav-link active" id="v-pills-<?=$model->id?>-tab" data-toggle="pill" href="#v-pills-<?=$model->id?>" role="tab" aria-controls="v-pills-home" aria-selected="true"><?=$model->title?></a>
 <?php else: ?>
 <a class="nav-link" id="v-pills-<?=$model->id?>-tab" data-toggle="pill" href="#v-pills-<?=$model->id?>" role="tab" aria-controls="v-pills-home" aria-selected="true"><?=$model->title?></a>
 <?php endif;endforeach;?>
</div>
</div>
</ul>
</nav>
<div class="col-lg-9 col-xs-12" data-scrollreveal="enter right over 2s after 0.5s">
<div class="tab-content" id="v-pills-tabContent">
<?php foreach ($models as $model):?>
<?php if($f2): $f2 = false; ?>
<div class="tab-pane fade show active" id="v-pills-<?=$model->id?>" role="tabpanel" aria-labelledby="v-pills-a-tab">
<div class="container">
<div class="row">
<div class="col-sm-12 features-details">
<img src="<?=Yii::$app->imagemanager->getImagePath($model->icon)?>" alt="icon">
<h4><?=$model->title?></h4>
<?=$model->description?>
<h5>Other details</h5>
<?=$model->other?>
</div>
</div>
</div>
</div>
 <?php else: ?>
<div class="tab-pane fade" id="v-pills-<?=$model->id?>" role="tabpanel" aria-labelledby="v-pills-b-tab">
<div class="container">
<div class="row">
<div class="col-sm-12 features-details">
<img src="<?=Yii::$app->imagemanager->getImagePath($model->icon)?>" alt="icon">
<h4><?=$model->title?></h4>
<?=$model->description?>
<h5>Other details</h5>
<?=$model->other?>
</div>
</div>
</div>
</div>
<?php endif;endforeach;?>
</div>
</div>
</div>

               

              

            
