
<?php
use yii\helpers\Html;
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
$first = true;
?>
<div class="carousel-inner">
<div class="slider-overlay">
<?php foreach ($models as $model):?>
<?php if($first): $first = false; ?>
<div class="carousel-item active">
<a href="<?=$model->link?>">
<img src="<?=Yii::getAlias('@url').'/banners/'.$model->image?>" alt="<?=$model->title?>">
</a>
</div>
<?php else: ?>
<div class="carousel-item">
<a href="<?=$model->link?>">
<img src="<?=Yii::getAlias('@url').'/banners/'.$model->image?>" alt="<?=$model->title?>">
</a>
</div>
<?php endif; ?>
<?php endforeach;?>
</div>
<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
<img src="<?=$directoryAsset ?>/images/arrow-left.png" alt="left">
</a>
<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
<img src="<?=$directoryAsset ?>/images/arrow-right.png" alt="right">
</a>
</div>

               

              

            
