<?php
use yii\helpers\Html;
?>
<section class="clients-area">
<div class="container">
<div class="row">
<div class="col-sm-12 clients-head">
<h2><span>Our</span> Brands</h2>
</div>
<div class="col-sm-12 owl-carousel-3 owl-slides no-padding">
<?php foreach ($model->nodeImages as $img):?>
<div class="item active">
<div class="row">
<div class="col-sm-12">
<figure><img src="<?=Yii::getAlias('@url').'/nodeimages/'.$img->node_image?>" alt="<?=$img->node_image_title?>"></figure>
</div>
</div>
</div>
<?php endforeach;?>
</div>
</div>
</div>
</section>


               

              

            
