<?php
use yii\helpers\Html;
?>
<div class="col-sm-12 owl-2 owl-theme owl-slides" data-scrollreveal="enter top over 2s after 1s">
<?php foreach ($models as $model):?>  
<div class="item">
<div class="row">
<div class="col-sm-12 client-area">
<div class="client-details">
<div class="row justify-content-center">
<div class="col-sm-2 profile-pic offset-3">
<img src="<?=Yii::getAlias('@url').'/testimonials/'.$model->image?>" alt="profile" class="rounded-circle">
</div>
<div class="col-sm-7 profile-name no-padding">
<h5><?=$model->title?></h4>
<h6><?=$model->des?></h6>
</div>
</div>
<div class="row">
<div class="col-sm-12 profile-disc">
<p><?=\yii\helpers\StringHelper::truncate($model->description,140,'...','UTF-8',false)?>
</div>
</div>
</div>
</div>
</div>
</div>
<?php endforeach;?>
</div>


               

              

            
