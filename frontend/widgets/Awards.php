<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use backend\models\Basic;

class Awards extends Widget implements \yii\base\ViewContextInterface
{         
public $model;
        public function init()
        {
        	parent::init();
            $this->model = Basic::find()->where(['type' => 'basic','link' => 'achieve'])->one();
        }
        public function run()
        {
    	   return $this->render('widget--awards', [
            'model' => $this->model,
        ]);

        }
        public function getViewPath()
        {
            return isset(Yii::$app->params['site.widgets']) ? Yii::getAlias(Yii::$app->params['site.widgets']) : Yii::getAlias('@frontend/views/widgets/');
        }
}
?>
