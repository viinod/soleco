<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use backend\models\Product;

class Products extends Widget implements \yii\base\ViewContextInterface
{         
public $model;
        public function init()
        {
        	parent::init();
            $this->model = Product::find()->where(['type' => 'product'])->all();
        }
        public function run()
        {
    	   return $this->render('widget--products', [
            'models' => $this->model,
        ]);

        }
        public function getViewPath()
        {
            return isset(Yii::$app->params['site.widgets']) ? Yii::getAlias(Yii::$app->params['site.widgets']) : Yii::getAlias('@frontend/views/widgets/');
        }
}
?>
