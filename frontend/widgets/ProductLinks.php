<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use backend\models\Product;

class ProductLinks extends Widget implements \yii\base\ViewContextInterface
{         
public $model;
        public function init()
        {
        	parent::init();
            $this->model = Product::find()->where(['type' => 'product'])->limit(5)->orderBy(['id' => SORT_DESC])->all();
        }
        public function run()
        {
    	   return $this->render('widget--product-links', [
            'models' => $this->model,
        ]);

        }
        public function getViewPath()
        {
            return isset(Yii::$app->params['site.widgets']) ? Yii::getAlias(Yii::$app->params['site.widgets']) : Yii::getAlias('@frontend/views/widgets/');
        }
}
?>
