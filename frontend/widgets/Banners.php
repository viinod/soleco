<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use backend\models\Banner;

class Banners extends Widget implements \yii\base\ViewContextInterface
{         
public $model;
        public function init()
        {
        	parent::init();
            $this->model = Banner::find()->where(['type' => 'banner','status' => 1])->all();
        }
        public function run()
        {
    	   return $this->render('widget--banners', [
            'models' => $this->model,
        ]);

        }
        public function getViewPath()
        {
            return isset(Yii::$app->params['site.widgets']) ? Yii::getAlias(Yii::$app->params['site.widgets']) : Yii::getAlias('@frontend/views/widgets/');
        }
}
?>
