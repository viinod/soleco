<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;

class Block extends Widget implements \yii\base\ViewContextInterface
{         
public $model;
public $id;
        public function init()
        {
        	parent::init();
            if (!is_null($this->id)) {
            $this->model = \backend\models\Block::find()->where(['id' => $this->id,'status' => 1])->one();        
            }
            else{
            throw new \yii\web\NotFoundHttpException("Block not found!");
                }
            
        }
        public function run()
        {
            $view = 'block--id-'.$this->id;
            if (!is_null($this->model)) {
               return $this->render($view, [
                'model' => $this->model,
            ]);                
            }
            else{
                return;
                }


        }
        public function getViewPath()
        {
            return isset(Yii::$app->params['settings']['site']['blocks']) ? Yii::getAlias(Yii::$app->params['settings']['site']['blocks']) : Yii::getAlias('@frontend/views/blocks/');
        }
}
?>
