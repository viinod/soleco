<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use backend\models\Partner;

class Partners extends Widget implements \yii\base\ViewContextInterface
{         
public $model;
        public function init()
        {
        	parent::init();
            $this->model = Partner::find()->where(['type' => 'partner','status' => 1])->all();
        }
        public function run()
        {
    	   return $this->render('widget--partners', [
            'models' => $this->model,
        ]);

        }
        public function getViewPath()
        {
            return isset(Yii::$app->params['settings']['site']['widgets']) ? Yii::getAlias(Yii::$app->params['settings']['site']['widgets']) : Yii::getAlias('@frontend/views/widgets/');
        }
}
?>
