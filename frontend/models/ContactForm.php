<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $type;
    public $size;
    public $subject = "Contact mail from ";
    public $body;
    //public $verifyCode;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'phone', 'type','size'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            ['body','safe'],
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail()
    {
        return Yii::$app->mailer->compose('contact-html',['name' => $this->name,'email' => $this->email,'phone' => $this->phone,'type' => $this->type,'size' => $this->size,'body' => $this->body])
            ->setTo(Yii::$app->params['smtp.email'])
            ->setFrom(Yii::$app->params['smtp.email'])
            ->setSubject($this->subject.$this->name)
            ->setTextBody($this->body)
            ->send();
    }
}
