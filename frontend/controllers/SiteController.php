<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\ContactForm;
use backend\models\Node;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('page--front');
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } 
        return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionSlug($slug)
    { 
        $key = \yii\helpers\Inflector::singularize($slug);
        if(Node::find()->where(['type' => $key])->exists()){
        
        $index = \backend\widgets\Template::widget(['category' => 'list','type' => $key]);    
        $models = Node::find()->where(['type' => $key])->all();
                 if (!is_null($models)) {
                  return $this->render($index, [
                      'models' => $models,
                  ]);      
                } }
        elseif($model = Node::find()->where(['slug'=>$slug])->one()){
        $view = \backend\widgets\Template::widget(['category' => 'detail','type' => $model]);    
  
                 if (!is_null($model)) {
                  return $this->render($view, [
                      'model' => $model,
                  ]);      
                } }
                else {
                throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
               }   

    }
    public function actionDetail($slug)
    { 

        $model = Node::find()->where(['slug'=>$slug])->one();
        if (!is_null($model)) {
        $view = \backend\widgets\Template::widget(['category' => 'detail','type' => $model]);    
        $seo = isset(Yii::$app->params['seo']['/node/'.$model->id]) ? Yii::$app->params['seo']['/node/'.$model->id] : "";  
                  return $this->render($view, [
                      'model' => $model,
                      'seo' => $seo,
                  ]);      
                } 
                else {
                throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
               }   

    }

    public function actionView($id)
    {
        $model = Node::find()->where(['id'=>$id])->one();
        $seo = isset(Yii::$app->params['seo']['/node/'.$model->id]) ? Yii::$app->params['seo']['/node/'.$model->id] : "";   
        return $this->render('view', [
            'model' => $model,
            'seo' => $seo,
        ]);
    }


    public function getViewPath()
    {
        return isset(Yii::$app->params['site.template']) ? Yii::getAlias(Yii::$app->params['site.template']) : Yii::getAlias('@frontend/views/list/');
    }
    public function actionCallback()
    {
             Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
             return ['success' => true];
    }

    public function actionError()
    {
    $exception = Yii::$app->errorHandler->exception;
    if ($exception !== null) {
        return $this->render('error', ['exception' => $exception]);
    }
    }
// public function beforeAction($action)
// {            
//     if ($action->id == 'detail') {
//         $this->enableCsrfValidation = false;
//     }

//     return parent::beforeAction($action);
// }
}
