<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@webroot/webassets';
    public $css = [
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.4.1/css/all.min.css',
        'https://fonts.googleapis.com/css?family=Assistant:200,300,400,600,700,800&display=swap',
        'css/style.css',
        'css/pe-icon-7-stroke.css',
        'css/page-efx.css',
        'css/owl.carousel.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.css',
        'css/responsive.css',

    ];
    public $js = [
        'https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.js',
        'js/classie.js',
        'js/modernizr.js',
        'js/main.js',
        'js/owl.carousel.js',
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset'
    ];
}
